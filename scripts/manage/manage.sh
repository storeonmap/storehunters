#!/bin/sh

deploy() {
    cd shophunter
    git pull
    forever restartall
}

backup_index() {
    cd data/proddata
    git pull
    elasticdump --input=http://localhost:9200/lbu --output=lbu_data.index --type=data
    elasticdump --input=http://localhost:9200/lbu --output=lbu_mapping.index --type=mapping
    elasticdump --input=http://localhost:9200/categories --output=categories_data.index --type=data
    elasticdump --input=http://localhost:9200/categories --output=categories_mapping.index --type=mapping
    git add categories_data.index categories_mapping.index lbu_data.index lbu_mapping.index
    git commit -m"Datadump of categories and lbu from prod server"
    git push    
}

import_index() {
    cd data/proddata
    git pull
    elasticdump --output=http://localhost:9200/lbu --input=lbu_mapping.index --type=mapping
    elasticdump --output=http://localhost:9200/lbu --input=lbu_data.index --type=data
    elasticdump --output=http://localhost:9200/categories --input=categories_mapping.index --type=mapping
    elasticdump --output=http://localhost:9200/categories --input=categories_data.index --type=data
}

install_tools() {
    yum -y update
    yum -y install git
    yum -y install screen
    curl -sL https://rpm.nodesource.com/setup | bash -
    yum -y install nodejs
    yum -y install npm
    npm install -g forever    
    npm install -g elasticdump
}

install_nodejs() {
    echo "nothing to do"
}

setup_codebase() {
    eval "$(ssh-agent -s)"
    ssh-add /root/.ssh/shophunter2    
    git clone 'git://github.com/niklaslind/shophunter.git'
}

start_nodejs() {
    cd ~/shophunter
    forever server.js
}

start_elastic() {
    echo "start elastic"
    screen -d -m ~/elasticsearch/current/bin/elasticsearch
}


install_elasticsearch() {
    yum -y update
    yum -y update
    yum -y install java-1.7.0-openjdk.x86_64
    mkdir -p elasticsearch
    cd elasticsearch
    wget "https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.3.4.tar.gz"
    tar -zxf elasticsearch-1.3.4.tar.gz
    ln -sf elasticsearch-1.3.4 current
}




$*
