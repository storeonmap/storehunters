#!/bin/zsh

tmux_init() {
    echo "Setup tmuxinator"
    brew install tmux
    sudo gem install tmuxinator
    ln -sf ${PWD}/shophunter.yml ~/.tmuxinator/shophunter.yml
    ls -l  ~/.tmuxinator
    tmuxinator list
}

tmux_start() {
    tmuxinator start shophunter
}

tmux_stop() {
    tmux kill-session -t shophunter
}


$*
