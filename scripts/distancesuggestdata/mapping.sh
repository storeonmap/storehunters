curl -X DELETE localhost:9200/cats
curl -X PUT localhost:9200/cats/
curl -X PUT localhost:9200/cats/cats/_mapping -d '{
    "cats": {
        "properties": {
            "location": {
                "type": "geo_point",
                "lat_lon": true,
                "geohash": true
            }
        }
    }
}'
