var LineByLineReader = require('line-by-line');
var request = require('request');
var lr = new LineByLineReader('kategorier.txt');

var resultMap = {}

lr.on('error', function (err) {
    
});

lr.on('line', function (line) {
    var indata = line.split('\t')
    for (var i = 0; i < indata.length; i++) {
        var currentMap = getCategoryMap(i)
        var currentWord = indata[i]
        if (indata[0] == 'Produktkategorier') 
            currentMap['category']=indata[i]
        else if (indata[0] == 'Id') 
            currentMap['id']=indata[i]
        else if (indata[0] == 'gammal branschkategori') 
            currentMap['oldcategory']=indata[i]
        else if (indata[0] == 'borttagen') 
            currentMap['borttagen']=indata[i]
        else if (indata[0] == 'Primär källa') 
            currentMap['primary']=indata[i]
        else if (currentWord != '')
            currentMap['synonym'].push(indata[i])
    }
});

lr.on('end', function () {
    var url = 'http://localhost:9200/categories/categories/'
    for (var key in resultMap){
        var v = resultMap[key]
        if(!v.borttagen)
            updateElastic(url+v.id, v)
    }
});


function getCategoryMap(i) {
    if (resultMap[i] !== undefined) return resultMap[i];
    else {
        resultMap[i] = {'synonym':[]}
        return resultMap[i]
    }
}




function updateElastic(url, data) {
    var d = JSON.stringify(data);
    console.log(d)
    request.post( {
        url: url,
        body: d},
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('OK: saved to Elastic: '+d );
            } else {
                console.log('err: '+error+", "+response.statusCode );
                console.log(response );
                console.log(body );
            }
        }
    );
}






