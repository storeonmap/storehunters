from fabric.api import *

@task
def env_nisse():
    """env-conf: nisse"""
    env.environment='nisse'
    env.key_filname = ["~/.ssh/id_rsa.pub"]
    env.roledefs = {
        'nodejs': ['root@88.80.172.68']
    }


@task
def env_lasse():
    """env-conf: lasse"""
    env.environment='lasse'
    env.key_filname = ["~/.ssh/id_rsa.pub"]
    env.roledefs = {
        'nodejs': ['root@88.80.172.73']
    }

@task
def env_localhost():
    """env-conf: local"""
    env.environment='localhost'
    env.key_filname = ["~/.ssh/id_rsa.pub"]
    env.roledefs = {
        'nodejs': ['nli@localhost']
    }


@task
def env_vagrant():
    """env-conf: vagrant"""
    env.environment='vagrant'
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)
    env.key_filename = result.split()[1]
    env.roledefs = {
        'nodejs': ['vagrant@127.0.0.1:2222']
    }


@task
@roles('nodejs')
def deploy():
    manage('deploy')

@task
@roles('nodejs')
def old_backupindex():
    manage('backupindex')

@task
@roles('nodejs')
def startelastic():
    manage('start_elastic')




@task
@roles('nodejs')
def install_all():
    sudo("mkdir -p /root/.ssh")
    put('keys/shophunter2', '/root/.ssh/shophunter2', use_sudo=True)
    put('keys/shophunter2.pub', '/root/.ssh/shophunter2.pub', use_sudo=True)
    sudo('ls -latr /root/.ssh')
    manage('install_tools')
    manage('install_nodejs')
    manage('install_elasticsearch')
    manage('setup_codebase')
    manage('start_nodejs')



@task
@roles('nodejs')
def shell():
    print 'How to connect: '
    print 'ssh <host>'
    print 'screen -ls'
    print 'sceen -r'
    print 'detach: ^a^d'
    open_shell("")
    
def manage(cmd):
    put('manage.sh')
    run('chmod +x manage.sh')
    sudo('./manage.sh %s'%cmd)

@task
@roles('nodejs')
def backup_index():
    with cd('/tmp'):
        run('elasticdump --input=http://localhost:9200/lbu --output=lbu_data.index --type=data')
        run('elasticdump --input=http://localhost:9200/lbu --output=lbu_mapping.index --type=mapping')
        run('elasticdump --input=http://localhost:9200/categories --output=categories_data.index --type=data')
        run('elasticdump --input=http://localhost:9200/categories --output=categories_mapping.index --type=mapping')
        with lcd('data/proddata'):
            get('lbu_mapping.index','.')
            get('lbu_data.index','.')
            get('categories_mapping.index','.')
            get('categories_data.index','.')

@task
@roles('nodejs')
def import_index():
    with lcd('data/proddata'):
        run('ls -latr')
        run('elasticdump --output=http://localhost:9200/shoppa --input=shoppa_mapping.index --type=mapping')
        run('elasticdump --output=http://localhost:9200/shoppa --input=shoppa_data.index --type=data')

        

        


