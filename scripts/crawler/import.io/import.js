#!/usr/bin/env node

var elasticsearch = require('elasticsearch');
// Connect to localhost:9200 and use the default settings
var client = new elasticsearch.Client();
var program = require('commander');
var crypto = require('crypto');
var moment = require('moment');
var fs = require('fs');


var now = moment().format('YYYYMMDD-hmmss');

program
    .version('0.0.1')
    .option('-f, --infile <inputfilename>', 'Import data from this file')
    .option('-j, --importjson', 'Import data as json')
    .option('-c, --importcsv', 'Import data as csv')
    .option('-d, --dryrun', 'Show data that will be sent to elasticsearch but send nothing')
    .option('-i, --import', 'Import data to elasticsearch')
    .parse(process.argv);



function saveArticle(data) {
    console.log("SAVE ART: "+JSON.stringify(data,null,4))
    client.index(
        {
            index: 'lbu',
            type: 'article',
            id: data.cid,
            body: data
        }).then(function (body) {
            console.log(body)
        }, function (error) {
            console.trace(error.message);
        }); 
}

function hash(s) {
//    return parseInt(crypto.createHash('md5').update(s).digest("hex"),16).toString(10);
    return crypto.createHash('md5').update(s).digest("hex").replace(/[a-z]/g,0).substring(1, 18);
}

function createArticleData(indata) {
    return {
        "url": indata.image_link[0],
        "articleName": indata.category[0]+' '+indata.category[1],
        "breadCrumb": "",
        "lbu": "4958118894313304241",
        "imageUrl":  indata.image[0],
        "price": indata.price[0],
        "textEx": "",
        "cid": hash(indata.image_link[0]),
        "crawl": "import.io",
        "imported": now        
    }
}


function csvToElastdata(data) {
    console.log('url='+data[4]);
    return {
        "url": data[4],
        "articleName": data[5],
        "breadCrumb": "",
        "lbu": "6249235443209092260", //Gatt
        "imageUrl":  data[10],
        "price": data[9],
        "textEx": data[8],
        "cid": hash(data[4]),
        "crawl": "import.io",
        "imported": now        
    }

}

function readCsv() {
    var parse = require('csv-parse');
    fs.readFile(program.infile, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        parse(data, {comment: '#'}, function(err, output){
            if (err) {
                return console.log('err: '+err);
            } else {
                for(d in output) {                    
                    var  elastData = csvToElastdata(output[d])
                    if (program.dryrun)
                        console.log(elastData)
                    if (program.import)
                        saveArticle(elastData)
                }
            }        
        });
    });
}


function readJson(filename) {
    var obj = JSON.parse(fs.readFileSync(program.infile, 'utf8'));

    console.log('Reading input json from: '+program.infile)

    var data = obj.data

    for (var d in data) {
        var elastData = createArticleData(data[d]);
        if (program.dryrun)
            console.log(elastData)
        if (program.import)
            saveArticle(elastData)
    }
}


function main() {
    if (program.importjson)
        readJson();
    else if (program.importcsv)
        readCsv();
}

main()




