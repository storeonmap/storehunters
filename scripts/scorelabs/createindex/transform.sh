#!/bin/bash


grep  phrases ../../../backup/proddata/lbu_data.index | head -n100 > data/originalData.json

cat data/originalData.json | ./transform.js > data/newData.json

elasticdump --output=http://localhost:9200/scorelab --input=data/newData.json --type=data

#curl -s -XPOST localhost:9200/_bulk --data-binary @data/newData.json;

