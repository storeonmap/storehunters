#!/usr/local/bin/node

var readEachLine = require('read-each-line')
 

var processLine = function(line) {
    if(line.charAt(0) == ',') {
          line = line.replace(',','')
      }
      outputData(JSON.parse(line));      
}


var flatCategories = function(categories) {
    var newCategories=[]
    for(cat in categories) {
        newCategories.push(categories[cat].category)
    }
    return newCategories
}

var flatCategoryProducts = function(categories) {
    var newProds=[]
    for(cat in categories) {
        var products=categories[cat].synonym
        for(s in products) {
            newProds.push(products[s])
        }
    }
    return newProds

}

var copyField = function(source, dest, fieldname) {
    dest['phrases_'+fieldname] = source[fieldname]
}

var outputData = function(indata) {
    var source = indata._source
    indata._index='scorelab'
    if(source.phrases) {
        source.phrase_categories=flatCategories(source.businessCategory)
        source.phrase_categoryproducts=flatCategoryProducts(source.businessCategory)
        var phrases = source.phrases                  
        copyField(phrases, source, 'geo')
        copyField(phrases, source, 'product')
        copyField(phrases, source, 'brand')
        copyField(phrases, source, 'value')
        copyField(phrases, source, 'blockMeta')
        copyField(phrases, source, 'geoMeta')
        copyField(phrases, source, 'productMeta')
        copyField(phrases, source, 'brandMeta')
        copyField(phrases, source, 'valueMeta')
        console.log("%j",indata)
        console.log(",")
      } 
}

var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
//  output: process.stdout
});




var result = []
rl.on('line', processLine)
//rl.on('close', function() {console.log("%j",result)})

//process.stdin.pipe(require('split')()).on('data', processLine)

//readEachLine('data/originalData.json', 'utf8', parseIndata )
//console.log(result)
