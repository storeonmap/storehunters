var parsedJSON = require('./data/nisse_lbu.json');

var count = {}


function increment(countMap, key) {
    if (!countMap[key]) countMap[key]=0
    countMap[key]++    
}


parsedJSON.forEach(function(c){  
    cat = c._source.businessCategory
    for (var k in cat) {
        increment(count, cat[k].category)
        cat[k].synonym.forEach(function(s) {
            increment(count, s)
        })
    }

});

for (var k in count) {
    str = '{"name": "'+k+'", "size": '+count[k]+'},'
    console.log(str)
}




