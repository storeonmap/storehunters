

curl -X POST "http://localhost:9200/myparentindex/myparent/_search" -d' {
   "query": { 
      "bool": {
         "should": [ 
            {
               "match":{
                  "name": {
                     "query": "ica",
                     "boost":1
                  }
               }
            },{ 
               "has_child": {
                  "type": "mychild",
                  "query": 
                  {
                     "match":{
                        "category": {
                           "query": "ica",
                           "boost":1
                        }
                     }
                  }                  
               }   
            }
         ]
      }
   }
}' | jsonlint 



