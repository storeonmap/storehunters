

# http://blog.tryolabs.com/2015/02/25/elasticsearch-analyzers-or-how-i-learned-to-stop-worrying-and-love-custom-filters/



curl -XDELETE  'http://localhost:9200/myindex/' 

curl -X POST http://127.0.0.1:9200/myindex/ -d'
    {
        "settings" : {
            "analysis" : {
                "analyzer" : {
                    "my_ngram_analyzer" : {
                        "tokenizer" : "my_ngram_tokenizer"
                    }
                },
                "tokenizer" : {
                    "my_ngram_tokenizer" : {
                        "type" : "edgeNGram",
                        "min_gram" : "3",
                        "max_gram" : "10",
                        "token_chars": [ "letter", "digit" ]
                    }
                }
            }
        },
  "mappings": {
    "test": {
      "properties": {
        "text": {
          "type": "string",
          "analyzer": "my_ngram_analyzer"
        }
      }
    }
  }
}'

curl -XGET 'http://localhost:9200/myindex/_analyze?analyzer=my_ngram_analyzer&text=cyklarnas' | jsonlint | grep token


