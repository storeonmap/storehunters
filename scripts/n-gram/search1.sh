

echo "Search: cykelhjälm"
curl -X POST "http://localhost:9200/myindex/_search" -d '{
    "query": {
        "match": {
           "text": "cykelhjälm"
        }
    }
}' | jsonlint | grep text


echo "Search: cyklar"
curl -X POST "http://localhost:9200/myindex/_search" -d '{
    "query": {
        "match": {
           "text": "cyklar"
        }
    }
}' | jsonlint | grep text
