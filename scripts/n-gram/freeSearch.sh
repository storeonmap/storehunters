echo "Search: $1"
curl -X POST "http://localhost:9200/myindex/_search" -d '{
    "size": 400, 
    "query": {
        "match": {"text": "'+$1+'"}
    }
}' | jsonlint | grep text
