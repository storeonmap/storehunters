



curl -X PUT "http://localhost:9200/myindex/words/1" -d '{ "text" : "cyklarnas" }'
curl -X PUT "http://localhost:9200/myindex/words/2" -d '{ "text" :"cykeln" }'
curl -X PUT "http://localhost:9200/myindex/words/3" -d '{ "text" : "hjälm" }'

curl -X POST "http://localhost:9200/myindex/_search" -d '{
    "query": {
      "query_string":{
        "fields": ["text"],
        "query": "*"
      }
    }
}' | jsonlint
