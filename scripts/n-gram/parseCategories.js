
var categoriesJson = require('./categories_data.json');

var result = {};

function parse(categories) {
    categories.forEach( function(c) {
        result[c._source.category] = c._source.category;
        var synonym = c._source.synonym;
        if(synonym) {
            synonym.forEach(function(s){
                result[s] = s;
            });
        }
    });
}

function generateElasticPut(categoryMap) {
    var index=0;
    for(var c in categoryMap) {
        console.log('curl -X PUT "http://localhost:9200/myindex/words/'+index+'" -d \'{ "text" : "'+c+'" }\'');
        index++;
    }
}

function generateElasticBatch(categoryMap) {
    var index=0;
    for(var c in categoryMap) {
        console.log('{ "create" : { "_index" : "myindex", "_type" : "words", "_id" : "'+index+'" } }');
        console.log('{ "text" : "'+c+'" }');
        index++;
    }
}


parse(categoriesJson);
//generateElasticPut(result);
generateElasticBatch(result);
