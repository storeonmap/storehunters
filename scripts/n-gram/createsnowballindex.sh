

# http://blog.tryolabs.com/2015/02/25/elasticsearch-analyzers-or-how-i-learned-to-stop-worrying-and-love-custom-filters/



curl -XDELETE  'http://localhost:9200/myindex/' 

curl -X POST http://127.0.0.1:9200/myindex/ -d'
    {
        "settings" : {
            "analysis" : {

            "analyzer" : {
                "snowball_analyzer" : {
                    "tokenizer" : "standard",
                    "filter" : ["standard", "lowercase", "my_snow"]
                }
            },
            "filter" : {
                "my_snow" : {
                    "type" : "snowball",
                    "language" : "Swedish"
                }
            }
        }
    },
  "mappings": {
    "test": {
      "properties": {
        "text": {
          "type": "string",
          "analyzer": "snowball_analyzer"
        }
      }
    }
  }
}'

curl -XGET 'http://localhost:9200/myindex/_analyze?analyzer=snowball_analyzer&text=cyklarnas' | jsonlint | grep token


