curl -X DELETE localhost:9200/stemming01





f3() {
curl -X PUT localhost:9200/stemming01 -d '{
    "index" : {
        "analysis" : {
                   "analyzer" : {
                              "my_search_analyzer" : {
                                            "type" : "custom",
                                            "tokenizer" : "standard",
                                            "filter" : ["standard", "lowercase", "mynGram"]
                              }
                   },
                   "search_analyzer" : {
                              "my_search_analyzer2" : {
                                            "type" : "custom",
                                            "tokenizer" : "standard",
                                            "filter" : ["standard", "lowercase", "mynGram"]
                              }
                   },
                   "filter" : {
                            "mynGram" : {
                                       "type" : "nGram",
                                       "min_gram" : 2,
                                       "max_gram" : 20
                            }
                   }
        }
    }
}'


curl -X PUT localhost:9200/stemming01/stemming01/_mapping -d '{
    "stemming01" : {
        "properties" : {
            "category" : {"type" : "string", "analyzer":"my_search_analyzer"}
        }
    }}'
}




f2() {
curl -X PUT localhost:9200/stemming01 -d '{
    "index" : {
        "index" : "stemming01",
        "type" : "stemming01",
        "analysis" : {
                   "index_analyzer" : {
                              "my_index_analyzer" : {
                                            "type" : "custom",
                                            "tokenizer" : "standard",
                                            "filter" : ["lowercase", "mynGram"]
                              }
                   },
                   "search_analyzer" : {
                              "my_search_analyzer" : {
                                            "type" : "custom",
                                            "tokenizer" : "standard",
                                            "filter" : ["standard", "lowercase", "mynGram"]
                              }
                   },
                   "filter" : {
                            "mynGram" : {
                                       "type" : "nGram",
                                       "min_gram" : 2,
                                       "max_gram" : 20
                            }
                   }
        }
    }
}'


curl -X PUT localhost:9200/stemming01/stemming01/_mapping -d '{
    "stemming01" : {
        "properties" : {
            "category" : {"type" : "string", "analyzer":"my_index_analyzer"}
        }
    }}'
}






f0() {

curl -X PUT localhost:9200/stemming01 -d '{
"index": 
  { "number_of_shards": 1,
    "analysis": {
       "filter": {
                "snowball": {
                    "type" : "snowball",
                    "language" : "Swedish"
                }
                 },
       "analyzer": { "a2" : {
                    "type":"snowball",
                    "language" : "Swedish",
                    "tokenizer": "standard",
                    "filter": ["lowercase", "snowball"],
"stopwords": [
 "och,det,att,i,en,jag,hon,som,han,på,den,med,var,sig,för,så,till,är,men,ett,om,hade,de,av,icke,mig,du,henne,då,sin,nu,har,inte,hans,honom,skulle,hennes,där,min,man,ej,vid,kunde,något,från,ut,när,efter,upp,vi,dem,vara,vad,över,än,dig,kan,sina,här,ha,mot,alla,under,någon,allt,mycket,sedan,ju,denna,själv,detta,åt,utan,varit,hur,ingen,mitt,ni,bli,blev,oss,din,dessa,några,deras,blir,mina,samma,vilken,er,sådan,vår,blivit,dess,inom,mellan,sådant,varför,varje,vilka,ditt,vem,vilket,sitta,sådana,vart,dina,vars,vårt,våra,ert,era,vilkas"
]
                    }
                  }
     }
  }
}'


curl -X PUT localhost:9200/stemming01/stemming01/_mapping -d '{
    "stemming01" : {
        "properties" : {
            "category" : {"type" : "string", "analyzer":"a2"}
        }
    }}'


}


f1() {

curl -X PUT localhost:9200/stemming01 -d '{
"index": 
  { "number_of_shards": 1,
    "analysis": {
       "filter": {
                "snowball": {
                    "type" : "snowball",
                    "language" : "Swedish"
                }
                 },
       "analyzer": { "a2" : {
                    "type":"custom",
                    "tokenizer": "standard",
                    "filter": ["lowercase", "snowball"]
                    }
                  }
     }
  }
}'


curl -X PUT localhost:9200/stemming01/stemming01/_mapping -d '{
    "stemming01" : {
        "properties" : {
            "category" : {"type" : "string", "analyzer":"a2"}
        }
    }}'


}

$*


#    "index" : {
#        "analysis" : {
#            "analyzer" : {
#                "my_analyzer" : {
#                    "type" : "snowball",
#                    "language" : "Swedish"
#                }
#            }
#        }
#    }
#}'


#  "mappings": {
#    "stemming01": {
#      "properties": {
#        "category": { 
#          "type": "string",
#          "fields": {
#            "swedish": { 
#              "type":     "string",
#              "analyzer": "swedish"
#            }
#          }
#        }
#      }
#   }
# }
#}'
