#!/bin/bash

sudo yum -y update

sudo yum -y install git
sudo yum -y install tmux


curl -O -sSL https://get.docker.com/rpm/1.7.0/centos-6/RPMS/x86_64/docker-engine-1.7.0-1.el6.x86_64.rpm
sudo yum -y localinstall --nogpgcheck docker-engine-1.7.0-1.el6.x86_64.rpm
sudo service docker start

sudo easy_install pip
sudo pip install -U docker-compose

sudo usermod -aG docker vagrant
sudo docker run -v /usr/local/bin:/target jpetazzo/nsenter

sudo mkdir -p  /usr/share/elasticsearch/data


sudo yum -y install epel-release
sudo yum -y install npm
