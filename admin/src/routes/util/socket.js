
var io;


exports.initSocket = function(server) {
    io = require('socket.io').listen(server);
    io.sockets.on('connection', function (socket) {
        console.log('Connected on socket');
        socket.on('message', function(msg){
            console.log("Server got message: "+msg);
            updateShop(msg);
            io.sockets.emit('message', msg);
        });
    });
}
