'use strict';
var SortedArray = require('collections/sorted-array');

exports.sortMapArrayOnField = (field, array) => {
    return new SortedArray.SortedArray(array,
        (a, b) => {
            return normalizeSwedishCharacters(a[field]) === normalizeSwedishCharacters(b[field]);
        },
        (a, b) => {
            return normalizeSwedishCharacters(a[field]).localeCompare(normalizeSwedishCharacters(b[field]));
        }
    ).toArray();
};

exports.sortArray = (array) => {
    return new SortedArray.SortedArray(array,
        (a, b) => {
            return normalizeSwedishCharacters(a) === normalizeSwedishCharacters(b);
        },
        (a, b) => {
            return normalizeSwedishCharacters(a).localeCompare(normalizeSwedishCharacters(b));
        }
    ).toArray();
};

exports.sortArrayUniq = (array) => {
    var a = exports.sortArray(array).map((n) => {
        return n.trim();
    });
    return a.filter((item, pos) => {
        return !item.match(/^[‧\u00A0 ]*$/) && (!pos || normalizeSwedishCharacters(item) !== normalizeSwedishCharacters(a[pos - 1]));
    });
    //return a;
};

exports.sortCommaSeparated = (s) => { // TODO: Remove this function
    return exports.sortArray(s.split(/, */)).join(', ').replace(/ och /ig, ' & ');
};

// nodejs does not handle swedish characters sort order :-(, or maybe it does now in node 5?

var normalizeSwedishCharacters = (s) => {
    return !s ? '' : s.toLocaleLowerCase().
        replace(/å/g, 'zz1').
        replace(/[äæ]/g, 'zz2').
        replace(/[øö]/g, 'zz3').
        trim();
};

    // Older hack
    //replace(/[áà]/g, 'a').
    //replace(/[éèëê]/g, 'e').
    //replace(/ü/g, 'u').
    //replace(/ø/g, 'ö').
    //replace(/°/g, '').
    //replace(/å/g, '°').
    //replace(/[äæ]/g, 'å').
    //replace(/°/g, 'ä').


exports.normalizeSwedishCharacters = normalizeSwedishCharacters;

// Remove?
//String.prototype.capitalize = (all) => {
//    if (all) {
//        return this.split(' ').map((e) => {
//            return e.capitalize();
//        }).join(' ');
//    } else {
//        return this.length > 1 ? this.charAt(0).toLocaleUpperCase() + this.slice(1).toLocaleLowerCase() : this;
//    }
//};
