var cookie = require( "cookie" )

exports.getQuery = function(req, res) {
    var queryJson = req.body;    
    if (queryJson.q || queryJson.categories) {
        setCookie(res, 'query', queryJson);
        console.log("got query from body: "+JSON.stringify(queryJson));
    } else {
        queryJson = getCookie(req, 'query');
        console.log("got query from cookie: "+JSON.stringify(queryJson));
    }
    return queryJson;
}




function setCookie(res, key, data) {
    var c = cookie.serialize(key, JSON.stringify(data));
    res.setHeader('Set-Cookie', c);
}

function getCookie(req, key) {
    var c =  (cookie.parse(req.headers.cookie)[key]);
    if (c === undefined) 
        return {}
    else 
        return JSON.parse(c);
}




