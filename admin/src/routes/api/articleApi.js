var elasticsearch = require('elasticsearch');
// Connect to localhost:9200 and use the default settings
var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
//  log: 'trace'
});
var sorter = require("../util/sorter");
var lbuApi = require("./lbuApi");
var request = require('request');
var requestify = require('requestify');
var async = require('async');
var _ = require('lodash');


exports.createArticle = function(req, res) {
   var data = req.body;
   if (!data.cid) data.cid = new Date().getTime();
   saveArticle(data, res);
};

exports.new = function (req, res) {
    var hit = {
        articleName: "",
        url: "",
        lbu: req.params.lbu,
        lbuName: req.params.lbuName,
        textEx: "",
        imageUrl: "",
        price: "",
        breadCrumb: req.params.breadCrumb ? req.params.breadCrumb : ""
    }
    lbuApi.getLbus(function (lbus) {
        //console.log("NEW ART: "+JSON.stringify(lbus,null,4))
        res.render('article',{hit: hit, lbus: lbus});
    })
}


exports.updateArticle = function(req, res) {
   var data = req.body;
   data.cid = req.params.id;
   saveArticle(data, res);
};

exports.getArticle = function(req, res) {
    var id = req.params.cid
    client.get({
        index: 'lbu',
        type: 'article',
        id: id
    }).then(function (body) {
        res.json(body)
        console.log("GET ART: "+JSON.stringify(body.hits,null,4))
        //res.render('article',{id:id})
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
};

exports.get = function (req, res) {
    var id = req.params.cid;
        request.get(
            'http://elasticsearch:9200/lbu/article/'+id,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var j = JSON.parse(body);
                    var r = j._source;
                    //r.q = q;
                    r.lbu= req.params.lbu
                    r.lbuName= req.params.lbuName
                    console.log("GET article: "+JSON.stringify(r,null,4));
                    r.deb = JSON.stringify(r,null,4)


                    res.render('article',{hit: r   });
                } else {
                    console.log('ERROR (exports.get) response from elastic: '+ response.statusCode+", "+error);
                    console.trace(error.message);

                    res.redirect("/articlelist/"+1+"/"+2)
                }
            }
        );

};

exports.getArticles = function(req, res) {
    console.log("ALBU: "+req.params.lbu)
    var lbuId = req.params.lbu
    var query = {
        index: 'lbu',
        type: 'article',
        'q': lbuId,
        'size':400
    };
    console.log("query:"+JSON.stringify(query))

    var lbu=[]
    lbu['cid']=req.params.lbu
    lbu['name']=req.params.lbuName
    var articles=[]
    client.search(query).then(function (body) {
        body.hits.hits.forEach(function (hit) {
            console.log(hit._source.articleName)
            articles.push(hit)
            hit['lbuSort'] = hit._source.articleName[0] // It's a list?
        });
            res.render('articlelist', {
                'articles': sorter.sortMapArrayOnField('lbuSort', articles),
                'lbu': lbu
            })
        }, function (error) {
            console.trace(error.message);
            res.status(error.status).json(error.message)
        });
};

exports.getArticlesSearch = function(req, res) {
    //console.log("ALBU: "+req.params.lbu)
    var lbuId = req.params.lbu
    var q = req.query.q;

    var query = {
        index: 'lbu',
        type: 'article',
        body: {query: {bool: {must: [{match: {lbu: req.params.lbu}}, {wildcard: {"_all":q+"*"}}]}}},
        'size':400
    };

    console.log("query:"+JSON.stringify(query))

    var lbu=[]
    lbu['cid']=req.params.lbu
    lbu['name']=req.params.lbuName
    var articles=[]
    client.search(query).then(function (body) {
        body.hits.hits.forEach(function (hit) {
            console.log(hit)
            articles.push(hit)
            hit['lbuSort'] = hit._source.articleName[0] // It's a list?
        });
        res.render('articlelist', {
            q: q,
            'articles': sorter.sortMapArrayOnField('lbuSort', articles),
            'lbu': lbu
        })
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
};


exports.deleteArticle = function(req, res) {
    var id = req.params.id
    client.delete({
        index: 'lbu',
        type: 'article',
        id: id
    }).then(function (body) {
        res.render(body)

    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
};


exports.deleteAllArticles = function(req, res) {
    console.log('enter deleteAllArticles');
    client.deleteByQuery({
        index: 'lbu',
        type: 'article',
        q:  '*'
    }).then(function (body) {
        console.log(body);
        res.json(body)
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
};

var deleteArticleById = function(id) {
    client.delete({
        index: 'lbu',
        type: 'article',
        id: id
    }).then(function (body) {
        console.log("deleted: article"+id);
    }, function (error) {
        console.log("could not delete article "+id+" : "+error.message);
    });
};


exports.deleteArticlesByLbu = function(req, res) {
    var lbu = req.params.lbu;
    
    var articlesQuery = {
        query: {        
            query_string : {
                fields : [ "lbu"],
                query : lbu
            }
        }       
    };

   var findArticlesByLbu = function(callback) {
      client.search({
         index: 'lbu',
         type: 'article',
         size: 10000,
         body: articlesQuery
      }, callback );      
   };
   

   var deleteArticles = function(data) {
      _(data.hits.hits)
         .forEach(function(item){
            client.delete({
               index: 'lbu',
               type: 'article',
               id: item._id
            }, function(err, data){
               console.log("Deleted article: %j", data);
            });            
         });                 
   };
   
   
   async.waterfall([
      findArticlesByLbu,
      deleteArticles      
      ]
   );

   
   
};

function splitAndTrim(str) {
   return (str) ? str.split(/[\s,]+/) : [];
}


function saveArticle(data, res) {
   data.crawl='manual';
   data.type='article';
   data.imported='';
   data.priowords= splitAndTrim(data.priowords);
   console.log("SAVE ART: "+JSON.stringify(data,null,4));
   client.index(
      {
         index: 'lbu',
         type: 'article',
         id: data.cid,
         body: data
      }).then(function (body) {
         res.json(body)
      }, function (error) {
         console.trace(error.message);
         res.status(500).json(error.message)
      }); 
}

exports.articleCount = function(req, res) {
    client.count({
        index: 'lbu',
        type: 'article',
        q:  '*'
    }).then(function (body) {
        console.log(body);
        res.json(body)
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });    
}
