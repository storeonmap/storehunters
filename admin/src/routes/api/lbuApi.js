var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch:9200',
  log: 'trace'
});
var sorter = require("../util/sorter");



exports.createLbu = function(req, res) {
    var data = req.body;
    console.log(data)
    if (!data.cid) data.cid = new Date().getTime()   
    saveLbu(data, res)
}


exports.updateLbu = function(req, res) {
    var data = req.body;
    data.cid = req.params.id
    saveLbu(data, res)
}


exports.getLbu = function(req, res) {
    var id = req.params.id
    client.get({
        index: 'lbu',
        type: 'lbu',
        id: id
    }).then(function (body) {
        res.json(body)
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
}

// Not REST
exports.getLbus = function(result) {
    var lbus = [];
    client.search({
        index: 'lbu',
        type: 'lbu',
        "size":400,"query":{"match_all":{}}
    }).then(function (body) {
        body.hits.hits.forEach(function (lbu) {
            lbus.push({
                id: lbu._source.cid,
                name: lbu._source.name,
                street: lbu._source.street
                //, selected: ""
            })
        });
        //console.log("GET 1LBUS: "+JSON.stringify(lbus,null,4))
        result(sorter.sortMapArrayOnField('name',lbus));
    }, function (error) {
        console.trace(error.message);
    });
}

exports.deleteLbu = function(req, res) {
    var id = req.params.id
    client.delete({
        index: 'lbu',
        type: 'lbu',
        id: id
    }).then(function (body) {
        res.json(body)
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
}


function saveLbu(data, res) {
    client.index(
        {
            index: 'lbu',
            type: 'lbu',
            id: data.cid,
            body: data
        }).then(function (body) {
            res.json(body)
        }, function (error) {
            console.trace(error.message);
            res.status(500).json(error.message)
        }); 
}



