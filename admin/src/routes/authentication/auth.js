


var usersByName = {}
var usersById = {}
var users =  [
    { id: 1, username: 'bob', password: 'secret', email: 'bob@example.com' },
    { id: 2, username: 'niklas', password: 'secret', email: 'bob@example.com' },
]



exports.initUsers = function() {
    users.forEach(function(u) {
        usersByName[u.username]=u;
        usersById[u.id]=u;
    });
}

exports.getUserById = function(id) {
    return usersById[id];
}


exports.getUser = function(username) {
    return usersByName[username];
}




exports.whoami = function (req, res) {
    var data = {"username": req.user.username}
    res.setHeader('Content-Type', 'application/json');
    res.end( JSON.stringify(data) );        
}
