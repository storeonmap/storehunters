
var request = require('request');

var baseUrl = 'http://elasticsearch:9200/'

exports.updateData = function(index, id, data) {
    var callback=
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('OK: saved to Elastic: '+d );
            } else {
                console.log('err: '+error);
                console.log(response );
                console.log(body );
            }
        }
    var d = JSON.stringify(data,null,4);
    request.put( {
        url: baseUrl+index+"/"+id,
        body: d},
                 callback
               );
}



exports.search = function (index, data, successcallback) {
    var callback = function (error, response, body) {
        if (!error && response.statusCode == 200) {
            successcallback(response, body, parseResult(body))
        } else {
            console.log('exports.search err for: '+JSON.stringify(data,null,4)+", "+error );
            console.log(response );
            console.log(body );
        }
    }    
    post(baseUrl+index+'/_search', data, callback)
}


exports.getById = function (index, id, successcallback) {
    var callback = function (error, response, body) {
        if (!error && response.statusCode == 200) {
            successcallback(response, body, [JSON.parse(body)._source])
        } else {
            console.log('err: '+error );
            console.log(response );
            console.log(body );
        }
    }    
    request.get(
        baseUrl+index+"/"+id,
        callback
    );     
}


function parseResult(body) {
    var data = JSON.parse(body);
    var hits = data.hits.hits;
    var result = [];
    hits.forEach(function (hit) {
        result.push(hit._source);
    });
    return result;
}

function post(url, data, callback) {
    var d = JSON.stringify(data);  // Why stringify ???
    request.post( {
        url: url,
        body: d},
                  callback
                );
}

