var currentquery = require('../util/currentquery');
var elasticsearch = require('../elasticsearch/elasticsearch')
var category = require('../categories/category');
var request = require('request');
var sorter = require("../util/sorter");



exports.searchLbu = function(httpreq, httpres) {
    var query = currentquery.getQuery(httpreq, httpres);
    var q = query.q ? query.q.toLowerCase() : '';
    var httpCallback = function(resp, body, hits) {
        var result = sorter.sortMapArrayOnField('name',hits.map(function(h) {return webify(h)}))
        //console.log('result lbu: '+JSON.stringify(result,null,4))
        httpres.render('list', {hits:result, q:q});
    }
    searchLbu(q, httpCallback)
}


function webify(hit) {
    hit.id = hit.cid;
    if (!hit.location) {
        addLocation(hit, 'missing', 'missing');
    }
    hit.categorynamelist = category.categoryNamesAsList(hit.businessCategory)
    //console.log('webify lbu: '+JSON.stringify(hit.categorynamelist))
    return hit
}

function addLocation(obj, lat, lon) {
    var location = {
        lat: obj.lat,
        lon: obj.lon
    }
    obj.location = location;
}


function searchLbu(q, searchLbuCallback) {    
    elasticsearch.search('lbu/lbu', lbuQuery(q), searchLbuCallback)
}




function queryJson(must, mustNot) {
    return {
        "size" : 500,
        "query": {
            "bool": {
                "must": must,
                "must_not": mustNot
            }
        }
    }    
}


function lbuQuery(q) {
    must = []
    mustNot = []
    if (q) {
        q.split(" ").forEach(function(w){
            must.push({ "wildcard": { "_all": w+"*" }})
            mustNot.push({ "match": { "phrases.block": w }})
        })        
    }
    query = queryJson(must, mustNot)
    console.log("q="+JSON.stringify(query))
    return query

}


// ----- TEST ---------


searchLbu('reparat holbe', 
          function(response, body, hits) {
              hits.forEach(function(h){console.log(h.name)})
          }
) 



