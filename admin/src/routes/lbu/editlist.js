'use strict';
/*eslint no-console: 0*/

var elasticsearch = require('../elasticsearch/elasticsearch');
var category = require('../categories/category.js');
var sorter = require('../util/sorter');
var request = require('request');
var _ = require('lodash');


var Phrases = function () {
    this.block = [];
    this.geo = [];
    this.product = [];
    this.brand = [];
    this.value = [];
    this.blockMeta = '';
    this.geoMeta = '';
    this.productMeta = '';
    this.brandMeta = '';
    this.valueMeta = '';
};


exports.get = function (req, res) {
    category.refreshCategories();
    var cid = req.query.cid;
    var q = req.query.q;
    request.get(
        'http://elasticsearch:9200/lbu/lbu/' + cid,
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var j = JSON.parse(body);
                var r = j._source;
                r.q = q;
                if (r.location == undefined) {
                    r.location = {lat: '', lon: ''};
                }
                console.log('GET LBU: ' + JSON.stringify(r, null, 4));
                createPhrasesStrings(r);

                r.kategorier = category.categoriesAsMultiselect(r.businessCategory);
                var sw = {};
                var catCats = {};
                for (var key in r.businessCategory) {
                    var cat = r.businessCategory[key].category.trim();
                    var catId = r.businessCategory[key].id;
                    catCats[catId]=cat;
                    for (var w in r.businessCategory[key].synonym) {
                        var word = r.businessCategory[key].synonym[w];
                        var prodIds = sw[sorter.normalizeSwedishCharacters(word)];
                        if (prodIds) {
                            prodIds.cat[cat] = catId;
                            prodIds.oneCatId = undefined;
                        } else {
                            sw[sorter.normalizeSwedishCharacters(word)] = {cat: {}, oneCatId: catId};
                            sw[sorter.normalizeSwedishCharacters(word)].cat[cat] = catId;
                        }
                        sw[sorter.normalizeSwedishCharacters(word)].word = word.trim();
                    }
                }
                var keys = sorter.sortArray(Object.keys(sw));
                var cp = [];
                for (var pw in keys) {
                    cp.push(sw[keys[pw]]);
                }
                r.catProds = cp;
                r.catProdsSize = cp.length;
                r.catCatsSize = Object.keys(catCats).length;
                //console.log('CP: ' + JSON.stringify(cp, null, 4));
                res.render('edit', {hit: r});
            } else {
                //console.log('ERROR (exports.get) response from elastic: ' + response.statusCode + ', ' + error);
                res.redirect('/searchlist?&__msg__=Reloaded page!__msg__');
            }
        }
    );
};

exports.update = function (req, res) {
    var d = req.body;
    d.phrases = getPhrasesLists(d);
    saveLBU(d);
    d.location = {lat: d.lat, lon: d.lon};
    //console.log('UPDATE LBU: '+JSON.stringify(d,null,4));
    d.kategorier = category.categoriesAsMultiselect(category.getLbuCategoriesFromBody(d));
    createPhrasesStrings(d);
    //res.redirect('/edit?cid=' + d.cid +'&__msg__=Saved LBU!__msg__');  // TODO UTF-8 encode
    res.redirect('/searchlist?__msg__=Saved LBU!__msg__');  // TODO UTF-8 encode
};


//When a category is updated, we need to update all LBUs for that category
exports.updateAllLbuCategories = function (categoryId) {
   var q = {'size': 200, 'query': {'query_string': {'query': categoryId}}};

   var callback = function(index, lang) {
      return function (response, body, hits) {
         hits.forEach(updateLBUCategories(index, lang));
      };
   };

   elasticsearch.search('lbu/lbu', q, callback('lbu', 'sv'));
   elasticsearch.search('lbu_en/lbu', q, callback('lbu_en', 'en'));

};

//REST-api-utility method: load all LBU and resave them.
//This allows you to programmatically upate all (or selected) LBUs
exports.updateAllLbu= function () {
    var q = {'size':200, 'query': {'query_string': {'query':'*'}} };

   var callback = function(index, lang) {
      return function(response, body, hits) { 
         hits.forEach(updateLBUCategories(index, lang));
      };
   };
   
   elasticsearch.search('lbu/lbu', q, callback('lbu', 'sv'));
   elasticsearch.search('lbu_en/lbu', q, callback('lbu_en', 'en'));
};


var updateLBUCategories = _.curry(function(index, lang, lbu) {
    var original = lbu.businessCategory;
    lbu.businessCategory = {};
    for(var categoryId in original) {
        lbu.businessCategory[categoryId] = category.all_categories[categoryId];
    }

   lbu.categoryProducts=generateI18nCategoryProducts(lang, lbu);
   lbu.categoryNames=getCategoryNames(lbu.businessCategory);

   elasticsearch.updateData(index+'/lbu', lbu.cid, lbu);
});


function saveLBU(body) {
    var lbuCategories = category.getLbuCategoriesFromBody(body);
    console.log('save LBU: '+JSON.stringify(lbuCategories,null,4));
    var elasticData = {
        cid: body.cid,
        name: body.name,
        businessCategory: lbuCategories,
        categoryProducts: getCategoryProducts(lbuCategories),
        categoryNames: getCategoryNames(lbuCategories),
        phrases: getPhrasesLists(body),
        street: body.street,
        postNumber: body.postNumber,
        postArea: body.postArea,
        phone: body.phone,
        url: body.url,
        logoUrl: body.logoUrl,
        location: {
            lat: parseFloat(body.lat),
            lon: parseFloat(body.lon)
        },
        webstore: body.webstore !== undefined
    };
    elasticsearch.updateData('lbu/lbu', body.cid, elasticData);
}

function generateI18nCategoryProducts(lang, lbu ) {
   return _.flattenDeep(
      _(_.keys(lbu.businessCategory))
         .map( function(categoryId) {
            return _(_.get(category.all_categories, [categoryId,'i18n','keywords'], []))
               .map(function(keyword){
                  return _.get(keyword, lang, '');
               })
               .value();         
         })
         .value()
   );
}


function getCategoryProducts(categoryMap) {
    var flat = [];
    for(var key in categoryMap) {
        for(var s in categoryMap[key].synonym) {
            flat.push(categoryMap[key].synonym[s]);
        }
    }
    return flat;
}

function getCategoryNames(categoryMap) {
    var flat = [];
    for(var key in categoryMap) {
        flat.push(categoryMap[key].category);
    }
    return flat;
}


var getPhrasesLists = function (body) {
    var phrases = new Phrases();
    Object.keys(phrases).forEach(function (key) {
        var val = '';
        if (key.match('.*Meta$')) {
            phrases[key] = body[key] ? body[key] : '';
        } else {
            //var cap = body[key+'phrases-cap'];
            val = body[key + 'phrases'];
            val = !val ? [] : sorter.sortArrayUniq(val.trim().replace(/‧\u00A0/g, '').split(/[\r\n]+/).map(function (ph) {
                var phrase = ph.replace(/\b[0-9]+\b/g, '');
                return phrase;
            }));
            phrases[key] = val;
        }
    });
    return phrases;
};

var createPhrasesStrings = function (body) {
    var p = !body.phrases ? new Phrases() : body.phrases;
    Object.keys(p).forEach(function (key) {
        var val = p[key];
        if (key.match('.*Meta$')) {
            //body[key] = val ? val.replace(/(http[^ ]+)/g,'<a href='$1'>$1</a>') : '';
            body[key] = val ? val : '';
        } else {
            if (!val) {
                body[key + 'phrases'] = '';
            } else {
                val = val.map(function (n) {
                    return '‧\u00A0' + n;
                });
                body[key + 'phrasesLength'] = val.length;
                body[key + 'phrases'] = sorter.sortArrayUniq(val).join('\r\n') + '\r\n';
            }
        }
    });
};


exports.delete = function (req, res) {
    var cid = req.query.cid;
    if (cid != null) {
        request.del('http://elasticsearch:9200/lbu/lbu/' + cid,
            function (error, response) {
                if (!error && response.statusCode == 200) {
                    console.log('Success Delete ' + cid);
                    res.redirect('/searchlist?&__msg__=Deleted LBU!__msg__');
                } else {
                    console.log('ERROR:  ' + response.statusCode + ', ' + +error);
                    res.end('ERROR:  ' + response.statusCode + ', ' + +error);
                }
            }
        );
    } else {
        res.redirect('/searchlist');
    }
};


exports.new = function (req, res) {
    category.refreshCategories();
    var d = {
        cid: new Date().getTime(),
        name: '',
        businessCategory: '',
        phrases: new Phrases(),
        street: '',
        postNumber: '',
        postArea: '',
        phone: '',
        url: '',
        logoUrl: '',
        kategorier: category.categoriesAsMultiselect({}),
        categoryids: {},
        categorynames: {},
        location: {
            lat: '',
            lon: ''
        },
        webstore:''
    };
    createPhrasesStrings(d);
    res.render('edit', {hit: d});
};

