'use strict';
/*eslint no-console: 0*/

var elasticsearch = require('../elasticsearch/elasticsearch');
var editlist = require('../lbu/editlist');
var sorter = require('../util/sorter');
//var sprintf = require('sprintf-js').sprintf;
var vsprintf = require('sprintf-js').vsprintf;


exports.findAllCategories = function (callback) {
    var data = {'size': 400, 'query': {'match_all': {}}};
    elasticsearch.search('categories/categories', data, callback);
};

exports.searchCategories = searchCategories_l;


exports.all_categories = {};

exports.refreshCategories = function () {
    exports.findAllCategories(
        function (response, body, hits) {
            hits.forEach(function (h) {
                exports.all_categories[h.id] = h;
            });
        }
    );
};
exports.refreshCategories();

exports.categoriesAsMultiselect = function (selectedCategories) {
    var kategorier = [];
    for (var key in exports.all_categories) {
        kategorier.push({
            id: key,
            name: exports.all_categories[key].category,
            selected: (selectedCategories[key] != null)
        });
    }
    return sorter.sortMapArrayOnField('name', kategorier);
};

exports.categoryNamesAsList = function (businessCategoryMap) {
    var kategorier = [];
    for (var key in businessCategoryMap) {
        if (!exports.all_categories[key]) {
            console.log('ERROR: could not find categoryKey: ' + key + ' in categories: ' + exports.all_categories);
        } else {
            kategorier.push({
                id: key,
                name: exports.all_categories[key].category
            });
        }
    }
    //console.log('kategorier: '+JSON.stringify(kategorier,null,4))
    return sorter.sortMapArrayOnField('name', kategorier);
};


exports.getLbuCategoriesFromBody = function (body) {
    var categories = {};
    var c = body.categories;
    if (c != null) {
        if (typeof c === 'string') {
            categories[c] = exports.all_categories[c];
        } else {
            c.forEach(function (c) {
                categories[c] = exports.all_categories[c];
            });
        }
    }
    return categories;
};





function searchCategories_l(q, callback) {
    var queryData = {
        'query': {
            'simple_query_string': {
                'fields': ['synonym', 'category'],
                'query': q
            }
        }
    };
    elasticsearch.search('categories/categories', queryData, callback);
}


function getCategoryById(id, callback) {
    elasticsearch.getById('categories/categories', id, callback);
}


function updateCategoryById(body) {
    var categoryJson = {
        id: body.id,
        category: body.category,
        synonym: body.synonym,
        i18n: body.i18n,
        borttagen: body.borttagen ? body.borttagen : '',
        oldcategory: body.oldcategory ?body.oldcategory : '',
        primary: body.primary ? body.primary : ''

    };
    exports.all_categories[body.id] = categoryJson;
    elasticsearch.updateData('categories/categories', body.id, categoryJson);
}


// WEB/RestApi -----------------------------

exports.getList = function (req, httpres) {
    var q = req.query.q;
    var sendResponseCallback = function (response, body, hits) {
        httpres.render('category/categorylist', {'hits': sorter.sortMapArrayOnField('category', hits), q: q});
    };
    if (q) {
        searchCategories_l(q, sendResponseCallback);
    } else {
        exports.findAllCategories(sendResponseCallback);
    }
};


exports.getSingle = function (req, httpres) {
    var id = req.params.id;
    getCategoryById(id, function (response, body, hits) {
        hits[0].synonym = sorter.sortArrayUniq(hits[0].synonym);
        var maxmax = 15;
        if (hits[0].i18n && hits[0].i18n.keywords) {
            var svMaxlen = 0;
            var enMaxlen = 0;
            hits[0].i18n.keywords.forEach((row) => {
                svMaxlen = row.sv && row.sv.length > svMaxlen ? row.sv.length : svMaxlen;
                enMaxlen = row.en && row.en.length > enMaxlen ? row.en.length : enMaxlen;
            });
            svMaxlen = svMaxlen > maxmax ? maxmax : svMaxlen;
            enMaxlen = enMaxlen > maxmax ? maxmax : enMaxlen;
            hits[0].synonym = hits[0].i18n.keywords.map((row) => {
                return vsprintf(`%-${svMaxlen}s| %-${enMaxlen}s| %s`, [row.sv, row.en, row.status]);
            });
        } else {
            maxmax = 28
            var maxlen = 0;
            hits[0].synonym.forEach((row) => {
                maxlen = row && row.length > maxlen ? row.length : maxlen;
            });
            maxlen = maxlen > maxmax ? maxmax : maxlen;
            hits[0].synonym = hits[0].synonym.map((row) => {
                return vsprintf(`%-${maxlen}s |  |`, [row]);
            });
        }
        //console.log('HITS2: ' + JSON.stringify(hits[0], null, 4));
        httpres.render('category/categoryedit', {'hits': hits[0]});
    });
};

exports.newCategory = function (req, httpres) {
    var id = new Date().getTime();
    httpres.render('category/categoryedit', {
        'hits': {
            id: id,
            category: '',
            synonym: [],
            i18n: {name: {sv: '', en: '', status: '#'}, keywords:{}}
        }
    });
};


exports.save = function (req, httpres) {
    //var keywords = !req.body.keywords ? '' : sorter.sortArrayUniq(req.body.keywords.trim().replace(/‧\u00A0/g, '').split(/[\r\n]+/));
    var synonym = !req.body.keywords ? [] : sorter.sortArrayUniq(req.body.keywords.trim().replace(/‧\u00A0/g, '').split(/[\r\n]+/)).map(k => {
        return k.split(/ *\| */)[0];
    });

    // new structure -> i18n = [ { sv:hund, en:dog, status: << '-', '?', '+', 'r' >> }, ];
    var i18n = !req.body.keywords ? [] : sorter.sortArrayUniq(req.body.keywords.trim().replace(/‧\u00A0/g, '').split(/[\r\n]+/)).map(k => {
        var c = k.split(/ *\| */);
        return {'sv': c[0], 'en': c[1], 'status': c[2] ? c[2] : ''};
    });
    console.dir('KW:' + i18n);
    req.body.synonym = synonym;
    req.body.i18n = {name: {sv: req.body.category, en: req.body.encategory, status: '#'}, keywords: i18n};
    updateCategoryById(req.body);
    editlist.updateAllLbuCategories(req.body.id);
    console.log('d: ' + JSON.stringify(req.body));
    httpres.redirect('/categories/' + req.body.id + '?__msg__=Category saved!__msg__');
};









// TEST -------------

/*
function testFindAllCategories() {
    exports.findAllCategories(searchCategoryCallback);
}

function testSearchCategory() {
//    searchCategories('Löpning', searchCategoryCallback)
//      searchCategories('löpning', searchCategoryCallback)
//    searchCategories('Löpni*', searchCategoryCallback)
// Does not work:  searchCategories('*pning', searchCategoryCallback)
//    searchCategories('vin*', searchCategoryCallback)
}

function testGetCategoryById() {
    getCategoryById(20, searchCategoryCallback);
}

var searchCategoryCallback = function (response, body, hits) {
    hits.forEach(function (hit) {
        console.log(hit);
    });
};

function testUpdateCategory() {
    updateCategoryById(999, 'testkategor', ['alfa', 'beta', 'gamma']);
}
*/




//testFindAllCategories()
//testSearchCategory()
//testGetCategoryById()
//testUpdateCategory() 
