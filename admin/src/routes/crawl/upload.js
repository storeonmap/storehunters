
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch:9200',
  log: 'trace'
});
var crypto = require('crypto');
var moment = require('moment');
var fs = require('fs');


exports.uploadCrawlJson = function(req, res) {
    var lbu = req.body.lbu
    var type = req.body.type
    console.log("Enter exports.uploadCrawlJson with lbu: "+lbu+", type="+type)
    var result = []
    var now = moment().format('YYYYMMDD-hmmss');
    var obj = JSON.parse(fs.readFileSync(req.files.displayImage.path, 'utf8'));
    var data = obj.data;
    for (var d in data) {
        if (data[d].article_name) {
            var elastData = createArticleData(data[d], lbu, type, now);
            saveArticle(elastData, result)
        }
    }
    res.json(result)
}

function saveArticle(data, result) {
    console.log('2: '+data.articleName)
    result.push(data)
    client.index(
        {
            index: 'lbu',
            type: 'article',
            id: data.cid,
            body: data
        }).then(function (body) {
            console.log("Saved: "+JSON.stringify(body))
            result.push(body)
        }, function (error) {
            console.trace(error.message);
            result.push(error)
        }); 
}

function hash(s) {
//    return parseInt(crypto.createHash('md5').update(s).digest("hex"),16).toString(10);
    return crypto.createHash('md5').update(s).digest("hex").replace(/[a-z]/g,0).substring(1, 18);
}

function createArticleData(indata, lbu, type, now) {
    return {
        "url": indata._pageUrl || "missing",
        "articleName": indata.article_name || "missing",
        "breadCrumb": indata.breadCrumb || "missing",
        "lbu": lbu || "missing",
        "imageUrl":  nullSafe(indata.article_image),
        "price": nullSafe(indata.price),
        "textEx": nullSafe(indata.article_description) || "missing",
        "cid": hash(indata._pageUrl) || "missing",
        "crawl": "import.io" || "missing",
        "type": type || "missing",
        "imported": now || "missing"       
    }
}

function nullSafe(array) {
    if (!array || array.length < 1)
        return "missing"
    else
        return array[0]
}





