module.exports = {
    'globals': {
        'describe': true,
        'it': true,
        'new': true,
        'target': true
    },
    'rules': {
        'indent': [2, 4],
        'quotes': [2, 'single'],
        'linebreak-style': [2, 'unix'],
        'semi': [2, 'always']
    },
    'env': {
        'es6': true,
        'node': true
    },
    'ecmaFeatures': {
        'newTarget': true,
        'blockBindings': true,
        'forOf': true,
        'arrowFunctions': true,
        'classes': true
    },
    parserOptions: {
        ecmaVersion: 6
    },
    'extends': 'eslint:recommended'
};

/* rules
 'no-console': 2
 */
