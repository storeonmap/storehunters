var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var auth = require('./routes/authentication/auth');
var BasicStrategy = require('passport-http').BasicStrategy;

var map = require('./routes/map/map');
//var demo = require('./routes/demo/demo');
var editlist = require('./routes/lbu/editlist');
var category = require('./routes/categories/category');
var monitor = require('./routes/util/monitor');
var socket = require('./routes/util/socket');
var lbu = require('./routes/lbu/lbu');
var lbuApi = require('./routes/api/lbuApi');
var articleApi = require('./routes/api/articleApi');
var upload = require('./routes/crawl/upload');
var path = require('path');

var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var multer = require('multer');
var errorHandler = require('errorhandler');
var fs = require('fs');

var app = express();

// all environments
app.set('port', process.env.SHPORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({
    resave: true,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: true,
        secure: false,
        maxAge: 360 * 24 * 60 * 60 * 1000
    },
    secret: 'uwotm8',
    rolling: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

app.use(multer());
app.use(express.static(path.join(__dirname, 'public')));

var connect        = require('connect')
var methodOverride = require('method-override')

// override with the X-HTTP-Method-Override header in the request
app.use(methodOverride('X-HTTP-Method-Override'))

// file upload for crawler
app.use(multer({
    dest: '/tmp/uploads/',
    inMemory: true,
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path)
    }
}));

app.post('/crawl/upload', upload.uploadCrawlJson)

app.get('/crawl/upload', function (req, res) {
  res.render('crawl/upload');
})

app.get('/crawl/upload/:cid', function (req, res) {
    res.render('crawl/upload', {"cid": req.params.cid, "type": req.query.type});
});



app.all('/map/*', ensureAuthenticated);
app.all('/search', ensureAuthenticated);
app.all('/searchlist', ensureAuthenticated);
app.all('/articlelist', ensureAuthenticated);
app.all('/whoami', ensureAuthenticated);
app.all('/edit', ensureAuthenticated);
app.all('/newlbu', ensureAuthenticated);
app.all('/categories', ensureAuthenticated);
app.all('/newcategory', ensureAuthenticated);



//app.all('/api/*', passport.authenticate('basic', { session: false }));
app.post('/api/lbu', lbuApi.createLbu)
app.put('/api/lbu/:id', lbuApi.updateLbu)
app.get('/api/lbu/updateall', editlist.updateAllLbu) //REST-api for programatically updating all LBU
app.get('/api/lbu/:id', lbuApi.getLbu)
app.delete('/api/lbu/:lbu/article', articleApi.deleteArticlesByLbu)
app.delete('/api/lbu/:id', lbuApi.deleteLbu)



app.post('/api/article', articleApi.createArticle)
app.put('/api/article/:id', articleApi.updateArticle)
app.get('/api/article/:id', articleApi.getArticle)
app.delete('/api/article', articleApi.deleteAllArticles)
app.delete('/api/article/:id', articleApi.deleteArticle)



app.get('/articlelist/:lbu/:lbuName', articleApi.getArticles )
app.get('/articlelist-search/:lbu/:lbuName', articleApi.getArticlesSearch )
//app.get('/newart', articleApi.new);
app.get('/newart/:lbu/:lbuName/:breadCrumb', articleApi.new);
app.get('/newart/:lbu/:lbuName', articleApi.new);
//app.get('/art', articleApi.get);
app.get('/art/:cid/:lbu/:lbuName', articleApi.get);

//app.get('/demo/cat', demo.lbuCat) // Not pushed yet
app.get('/newcategory', category.newCategory )
app.get('/categories/:id', category.getSingle )
app.post('/categories/:id', category.save )
app.get('/categories', category.getList )

app.all('/searchlist', lbu.searchLbu);
app.all('/searchmap', map.search);
app.get('/newlbu', editlist.new);
app.post('/newlbu', editlist.update);
app.get('/edit', editlist.get);
app.post('/edit', editlist.update);
app.get('/delete', editlist.delete);
app.get('/monitor', monitor.monitor);
app.get('/', function(req, res) {res.redirect('/searchlist')});

app.get('/article', function (req, res) {
  res.render('article');
})


auth.initUsers();


passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    done(null, auth.getUserById(id));
});


passport.use(new LocalStrategy(
    function(username, password, done) {
        console.log("Auth: LocalStrategy")
        process.nextTick(function () {
            var user = auth.getUser(username);
            if ((user != null) && password == 'sh0pp3r')
                return done(null, user);
            else
                return done(null, false);
        });
    }
));

passport.use(new BasicStrategy(
    function(username, password, done) {
        console.log("Auth: BasicStrategy")
        if (username == "bob" && password == "sh0pp3r")
            return done(null, {})
        else
            return done(null, false);
    }
));



app.get('/login', function(req, res){
    res.render('login', { user: 'hej' });
});

app.post('/login', passport.authenticate('local', { successRedirect: '/searchlist',
    failureRedirect: '/login' }));

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        req.session.roll = req.session.roll ? 0 : 1
        return next();
    }
    console.log("Session timed out: "+JSON.stringify(req.session,null,4))
    res.redirect('/login');
}

socket.initSocket(app.httpServer);


// error handling middleware should be loaded after the loading the routes
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
