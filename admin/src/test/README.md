# Frisbyjs tests

http://frisbyjs.com/

## Install

npm install -g frisby
npm install -g jasmine-node


## Run tests

jasmine-node frisby_specs/lbu_spec.js
jasmine-node frisby_specs/article_spec.js
