var frisby = require('frisby');
var fs = require('fs');
var path = require('path');



frisby.globalSetup({ // globalSetup is for ALL requests
    request: {
        headers: { 'Authorization': 'Basic Ym9iOnNoMHBwM3IK' }
    }
});

var getJSON = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'lbu1.json'), 'utf-8'));


//-----------  CLEANUP TEST DATA -----
frisby.create('Cleanup, not a test')
    .delete('http://127.0.0.1:3000/api/lbu/testid1')
    .toss();

frisby.create('Check that LBU is deleted before tests')
    .get('http://127.0.0.1:3000/api/lbu/testid1')
    .expectStatus(404)
    .toss();

//-----------------------------------




frisby.create('PUT LBU')
    .put('http://127.0.0.1:3000/api/lbu/testid1',
         getJSON
         )
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .toss();

frisby.create('Check that LBU is deleted before tests')
    .get('http://127.0.0.1:3000/api/lbu/testid1')
    .expectStatus(200)
    .toss();
