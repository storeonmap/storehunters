var frisby = require('frisby');
var fs = require('fs');
var path = require('path');



var getJSON = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'article1.json'), 'utf-8'));


//-----------  CLEANUP TEST DATA -----
frisby.create('Cleanup, not a test')
    .put('http://127.0.0.1:3000/api/article')
    .toss();

frisby.create('Cleanup, not a test')
    .delete('http://127.0.0.1:3000/api/article/testid1')
    .toss();

frisby.create('Check that article is deleted before tests')
    .get('http://127.0.0.1:3000/api/article/testid1')
    .expectStatus(404)
    .toss();

//-----------------------------------




frisby.create('PUT Article')
    .put('http://127.0.0.1:3000/api/article/testid1',
          { cid: "testid123", name: "Test ARTICLE 1"}
         )
    .expectStatus(200)
    .expectHeaderContains('content-type', 'application/json')
    .toss();

frisby.create('GET Article')
    .get('http://127.0.0.1:3000/api/article/testid1')
    .expectStatus(200)
    .toss();
