
var me = new google.maps.LatLng(59.366834, 17.950799);

var marker;

var map;
var markers = {};
var items = {};
var infoWindows = {};
var currentuser = "unknown";



$.getJSON( "/whoami", function( data ) {currentuser=data.username;});


function initInfoWindow(id) {
    infowindow = new google.maps.InfoWindow({
        content: 'Loading...'
    });
    infoWindows[id]=infowindow;
    var marker = markers[id];
    google.maps.event.addListener(marker, 'click', function() {
        setInfoWindowContent(id);
        infowindow.open(map,this);
    });
}

function setInfoWindowContent(id) {
    var item = items[id];
    infowindow = infoWindows[id];
    var saveId = "save"+item.id;
    var kategorier = [];
    for(d in item.businessCategory){kategorier.push(item.businessCategory[d]);};
    infowindow.setContent('<div>'+
                          'Namn: <input id="name'+item.id+'" type="text" name="name" value="'+item.name+'"><br>'+
                          'Kategorier: <input id="category'+item.id+'" type="text" name="Kategori" value="'+kategorier+'"><br>'+
                          'Street: <input id="street'+item.id+'" type="text" name="street" value="'+item.street+'"><br>'+
                          'Post: <input id="vicinity'+item.id+'" type="text" name="Adr" value="'+item.postNumber+' '+item.postArea+'"><br>'+
                          'Phone: <input id="street'+item.id+'" type="text" name="street" value="'+item.phone+'"><br>'+
                          'Url: <input id="street'+item.id+'" type="text" name="street" value="'+item.url+'"><br>'+
                          'lon: <input id="lon'+item.id+'" type="text" name="lon" value="'+item.location.lon+'"><br>'+
                          'lat: <input id="lat'+item.id+'" type="text" name="lat" value="'+item.location.lat+'"><br>'+
                          'Claimed: '+item.claimed+'<br>'+
                          '<input id="id'+item.id+'" type="hidden" name="id" value="'+item.id+'"><br>'+
                          '<button id="move'+item.id+'" >Använd min position</button>'+
                          '<button id="'+saveId+'" onClick="saveMe(\''+item.id+'\');">Spara</button>'+
                          '</div>'
                         );
}



function getValue(fieldName, id) {
    return document.getElementById(fieldName+id).value;
}


function initialize() {
    var mapOptions = {
        zoom: 13,
        center: me
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);

    new google.maps.Marker({
        map:map,
        draggable:true,
        animation: google.maps.Animation.DROP,
        position: me,
        title: 'Me',
        icon: '/images/me-icon.png'
    });

    initMarkers('*');
}




function resetMarkers(mymarkers) {
    for (var key in mymarkers){
        mymarkers[key].setMap(null);
    }
    markers = {};
    items = {};
    infoWindows = {};
}

function initMarkers(searchstring) {
    $.getJSON( "/searchmap?q="+searchstring, function( data ) {
        $.each( data, function( i ) {
            var item = data[i]
            if (item.location) {
                console.log(item.name)
                var p = new google.maps.LatLng(item.location.lat, item.location.lon);

                marker = new StyledMarker({
                    styleIcon:new StyledIcon(
                        StyledIconTypes.MARKER,
                        {color:"#ff0000",text:"A"}
                    ),
                    position:p
                    ,map:map
                });

                id = item.id;
                markers[id]=marker;
                items[id]=item;
                initInfoWindow(id);
            }
        });
    });
}

google.maps.event.addDomListener(window, 'load', initialize);


