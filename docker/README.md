

# Docker, Docker Machine and Docker Compose

This document describes how to install and run schmapz locally and
remote using Docker:

- prereq.
- build and run Dockers locally
- build and run Dockers remote
- development w continous deploy locally
- deploy to remote

## Prerequisits

Install:
 - Docker (OSX boot2docker: https://docs.docker.com/installation/mac/)
 - docker-machine: https://docs.docker.com/machine/
 - docker-compose: http://docs.docker.com/compose/install/


Create local path for persistant elasticsearch data:

    mkdir -p /usr/share/elasticsearch/data
 
 
## Start and deploy local dev

    cd docker
    docker-machine create -d virtualbox dev
    eval "$(docker-machine env dev)"
    docker-machine ls 

    docker-compose build
    docker-compose up -d
    docker ps
    
The above will
 1) Start a docker-machine as your local docker host named "dev"
    - docker-machine ls will give you IP
    
 2) Start the following Dockers on "dev":
    - admin (nodejs on port: 80)
    - schmapz (nodejs on port: 3000)
    - elasticsearch
    - redis

## Local development

This will just work, because:
docker-compose.yml: ../schmapz/src:/src will map your local disk to Docker

## Deploy to remote machine

    cd docker
    eval "$(docker-machine env nli-docker-host)"
    docker-compose build 
    docker-compose up -d 
    

## Some nice commands

    docker-machine create -d virtualbox dev //create new machine
    eval "$(docker-machine env dev)" //set ACTIVE machine
    docker-machine ls //list machines

    docker-compose build //build docker images
    docker-compose up -d //start dockers on active machine
    docker ps //list running dockers on active machine

    //logfiles
    docker logs -f <containerid>

    //Stop and remove all Dockers on active machine
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)

    //Delete local machine
    docker-machine rm dev
