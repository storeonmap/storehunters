
# Backup and restore elasticsearch-data

Data is stored on S3 and downloaded to local backup/proddata folder
before imported to elasticsearch.

## Init scripts

    cd backup
    npm install


## Get data from S3

    gulp downloadFromS3

## Import data to local elasticsearch

    gulp getProdDataFromS3
    curl -XDELETE 'localhost:9200/categories?pretty'
    curl -XDELETE 'localhost:9200/lbu?pretty'
    ./createIndex.sh
    ./import.sh importToElasic 


## Import data to remote elasticsearch

    ./import.sh importToRemoteElasic <ip>


## Export elasticsearch data from ElasticSearch to disk

    ./import.sh exportElasic


## Upload data to S3

    gulp uploadToS3



