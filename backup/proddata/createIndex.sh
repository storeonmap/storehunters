
curl -X PUT localhost:9200/lbu/
curl -X PUT localhost:9200/lbu/lbu/_mapping -d '{
    "lbu": {
        "properties": {
            "location": {
                "type": "geo_point",
                "lat_lon": true,
                "geohash": true
            }
        }
    }
}'
