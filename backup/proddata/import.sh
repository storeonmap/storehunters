
importToElasic() {
    elasticdump --output=http://localhost:9200/lbu --input=lbu_mapping.index --type=mapping  
    elasticdump --output=http://localhost:9200/lbu --input=lbu_data.index --type=data  --bulk=true
    elasticdump --output=http://localhost:9200/categories --input=categories_mapping_eng.index --type=mapping  
    elasticdump --output=http://localhost:9200/categories --input=categories_data_en.index --type=data  --bulk=true
    elasticdump --output=http://localhost:9200/lbu_en --input=lbu_mapping_en.index --type=mapping  
    elasticdump --output=http://localhost:9200/lbu_en --input=lbu_data_en_big.index --type=data  --bulk=true

}


importToRemoteElasic() {
    echo "Importing to $1"
    elasticdump --output=http://$1:9200/lbu --input=lbu_data.index --type=data
    elasticdump --output=http://$1:9200/lbu --input=lbu_mapping.index --type=mapping
    elasticdump --output=http://$1:9200/categories --input=categories_data.index --type=data
    elasticdump --output=http://$1:9200/categories --input=categories_mapping.index --type=mapping
}


exportElasic() {
    elasticdump --input=http://localhost:9200/lbu --output=lbu_data.index --type=data
    elasticdump --input=http://localhost:9200/lbu --output=lbu_mapping.index --type=mapping
    elasticdump --input=http://localhost:9200/lbu_en --output=lbu_en_data.index --type=data
    elasticdump --input=http://localhost:9200/lbu_en --output=lbu_en_mapping.index --type=data
    elasticdump --input=http://localhost:9200/categories --output=categories_data.index --type=data
    elasticdump --input=http://localhost:9200/categories --output=categories_mapping.index --type=mapping
}


$*


