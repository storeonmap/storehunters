# Shophunter


Directories:

- admin : Nodejs-app, Admin-GUI for LBU etc  
- schmapz : Nodejs-app, End-user search GUI
- docker : Scripts for deploy apps via docker
- scripts : various old stuff, could probably removed?


--------------------------------------------------------------------------------

## Install and run locally manually

Install: nodejs, elasticsearch, redis


    npm install --save watchify


--------------------------------------------------------------------------------
    
### Run admin locally manually

    cd admin/src
    export SHPORT=80;nodemon app.js


### Run schmapz locally manually

    cd schmapz/src
    ./manage.sh watchify_start

    cd schmapz/src
    export SCHMAPZ_PORT=3000;nodemon app.js


--------------------------------------------------------------------------------

## Install via docker (locally and remote)

see docker/README.md:

1) Install locally
2) Install on remote server
3) Local development
4) Deploy to remote


--------------------------------------------------------------------------------

## Backup and restore data

see backup/README.md

--------------------------------------------------------------------------------

### IP for elasticsearch in your hostfile

Docker will automatically add the IP to Dockerized Elasticsearch to
the Dockers hostfiles.
But if you run locally you need to add:

    /etc/hosts
    127.0.0.1       elasticsearch

--------------------------------------------------------------------------------












Access lasse & nisse
------------------------

### public ssh key 

    cat ~/.ssh/id_rsa.pub | pbcopy
    lasse, nisse... >> .ssh/authorized_keys
    
### /etc/hosts
    88.80.172.68 nisse
    88.80.172.73 lasse


### ssh
    ssh root@lasse
    
    
Deploy
------

    ssh root@lasse
    screen -ls #list detached screens
    screen -r nodejs #reattach to nodejs
    ^C #break forever-process
    git pull
    forever server.js
    ^a^d  #this will detach screen 


Running on non 80 port environment
----------------------------------

(t)csh: setenv SHPORT 3000
bash:   export SHPORT=3000
