var templateSource = document.getElementById('results-template').innerHTML;
var template = Handlebars.compile(templateSource);
var resultsPlaceholder = document.getElementById('results');

var search = function(cid) {
    $.post("/api/scorelab2/article",
        {
            cid: cid,
            q: document.getElementById("searchfield").value,
            name: document.getElementById("name").value,
            categoryNames: document.getElementById("categoryNames").value,
            categoryProducts: document.getElementById("categoryProducts").value,
            'phrases.product': document.getElementById("phrases.product").value,
            'phrases.brand': document.getElementById("phrases.brand").value,
            'phrases.value': document.getElementById("phrases.value").value,
            'article': document.getElementById("article").value
        },
        function (data) {
            resultsPlaceholder.innerHTML = template(data);
        });
    return true;
};

function searchOnClick(event) {
    event.preventDefault();
    return search();
}

document.getElementById("searchbox").addEventListener("submit", searchOnClick, false);

function searchArticles(cid) {
    return search(cid);
}

Handlebars.registerHelper("inc", function (value, options) {
    return parseInt(value) + 1;
});


$("#clickme").click(function () {
    $("#parameters").toggle("fast", function () {
    });
});
