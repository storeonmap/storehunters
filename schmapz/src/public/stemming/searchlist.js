var templateSource = document.getElementById('results-template').innerHTML;
var template = Handlebars.compile(templateSource);
var resultsPlaceholder = document.getElementById('results');

function searchOnClick(event)  {
    event.preventDefault();
    $.get( "/api/stemming/stemming01", 
           {q:document.getElementById("searchfield").value},
            function( data ) {
                resultsPlaceholder.innerHTML = template(data);
            });     
   return true;
}
 

document.getElementById("searchbox").addEventListener("submit", searchOnClick, false);
 

