 

(function() {

   var async=require('async');
   var request=require('browser-request');
   var _=require('lodash');
   var Handlebars=require('handlebars');

   var scoreSettingsTool=require('./scoreSettings.js');

   var templateSource = document.getElementById('results-template').innerHTML;
   var template = Handlebars.compile(templateSource);
   var resultsPlaceholder = document.getElementById('results');

   var lbuMap = {};
   var apiUrl='/api/scorelab4/api/v4/lbu/search/';

   var formParams = {};
   window.formParams = formParams;
   formParams['nearme'] = localStorage.getItem('shmapz.nearme') === 'true';
   formParams['webstore'] = localStorage.getItem('shmapz.webstore') === 'true';
   formParams['q'] = localStorage.getItem('shmapz.q');
   var wasMap = undefined;
   var nowMap = undefined;

   var myTimer;
   function clock(msg, startOrStop) {
      var d = new Date();
      if('start' === startOrStop)
         myTimer=d;
      else
         console.log(msg,d-myTimer,' millisecs');
   }

   function search(cid) {
      $.mobile.loading("show", {textVisible: false, html: '<div class="z-loader"><img src="/img/ring-deep-purple.gif"/></div>'});
      async.parallel(
         [
            asyncQuery(apiUrl+'article', getParameterMap('articleFields')),
            asyncQuery(apiUrl+'collection', getParameterMap('collectionFields')),
            asyncQuery(apiUrl+'lbu', getParameterMap('lbuFields'))
         ]
         , processQueryResultList);
   }


   function getParameterMap(key) {
      var q=formParams.q;
      var fullScoreSettings=scoreSettingsTool.getScoreSettings();
      var scoreSettings=fullScoreSettings[key] || {};
      return {
         parameterMap: {
            scoreSettings:scoreSettings,
            q:q,
            lang:getLang()
         }
      };
   }

   function getLang() {
      var parts = window.location.pathname.split('/');
      return (parts.length > 2 && parts[2] === 'en') ? 'en' : 'sv';
   }

   function asyncQuery(url, parameterMap) {
      if(!parameterMap) parameterMap={};
      //console.log("PARAM: %j", parameterMap);
      return function(callback) {
         request({
            method:'POST',
            url:url,
            json:parameterMap
         }, callback);
      };
   }

   function trimUrl(u) {
      return u && u !== 'missing' ? u.replace(/https?:\/\//, '').replace(/\/$/, '') : '';
   }

   function processQueryResultList(asyncErr, asyncResult) {
      if (asyncErr) console.log('asyncErr', asyncErr);
      var result = {
         hitList:[],
         hitMap:{}
      };
      processLbuResult(result, asyncResult[2]);
      processLbuCollectionResult(result, asyncResult[1]);
      processLbuArticleResult(result, asyncResult[0]);
      sort(result);

      _.forEach(result.hitList, function(r) {
         addDeepLink(r);
         if (r.lbu) {
            r.lbu.displayUrl = trimUrl(r.lbu.url);
            r.lbu.postArea = _.capitalize(r.lbu.postArea);
         }
      });

      window.hitList = result.hitList;

      renderMapSearchResult(result.hitList);
      renderSearchResult(result.hitList);
      $.mobile.loading( "hide" );
   }

   function getSearchWordsAsArray() {      
      return formParams.q
         ? _.lowerCase(formParams.q).split(' ')
         : [];
   }
   
   
   function sort(result) {
      _.forEach(result.hitList, function(r) {
         addDistance(r.lbu);
      });
      result.hitList = _.sortBy(result.hitList, function (hit) {
         return _.get(hit, 'prio',2) + '-' + _.get(hit, 'lbu.distance',0);
      });
   }
   
   function processLbuArticleResult(result, asyncResult) {
      _(getHits(asyncResult))
         .forEach(function(hit){
            var article = hit._source;
            truncText(article);
            addHitToResult(article, 'article', result);
         });
   }


   function truncText(article) {
      article.textEx = article.textEx.length > 140 ? article.textEx.slice(0,140)+' ...' : article.textEx;
      article.textEx = article.textEx.replace(/missing|Beskrivning/, '');
   }

   function addHitToResult(article, type, result) {
      var lbuId = article.lbu;
      var lbu = lbuMap[lbuId];
      var oldLbuHit = result.hitMap[lbuId];
      var prio = getPrio(article);
      var newLbuHit = {
         lbu:lbu,
         articleHit:article,
         type:type,
         prio: prio
      };
      if (oldLbuHit && (oldLbuHit.prio >= prio)) {
         oldLbuHit.articleHit=article;
         oldLbuHit.type='article';
         oldLbuHit.prio = prio;
      } else if (filterHit(lbu)) {
         if (!oldLbuHit) {
            result.hitList.push(newLbuHit);
            result.hitMap[lbuId] = newLbuHit;
         }
      }
   }

   function getPrio(article) {
      var priowords =
             _(_.get(article, 'priowords', []))
             .map(_.downCase)
             .value();      
      var matchingPrioWords = _.intersection(getSearchWordsAsArray(), priowords);
      console.log("priowords2, matchingPrioWords",priowords, matchingPrioWords);
      return _.isEmpty( matchingPrioWords ) ? 2 : 1;
   }
   
   function addDistance(lbu) {
      if (formParams.nearme && lbu) {
         lbu.distance = calculateDistance(lbu);
      }      
   }

   function processLbuCollectionResult(result, asyncResult) {
      _(getHits(asyncResult))
         .forEach(function(hit){
            var article = hit._source;
            truncText(article);
            addHitToResult(article, 'collection', result);
         });
   }
   
   function processLbuResult(result, asyncResult) {
      _(getHits(asyncResult))
         .forEach(function(hit){
            var lbuId = hit._source.cid;
            if (!result.hitMap[lbuId]) {
               var lbu = lbuMap[lbuId];
               var newLbuHit = {
                  lbu:lbu,
                  type:'lbu',
                  matchingFields: getMatchingFields(hit),
                  prio: 2
               };
               result.hitMap[lbuId] = newLbuHit;
               if (filterHit(lbu)) {
                  result.hitList.push(newLbuHit);
               }
            }
         });
   }


   function isPostArea(lbu, area) {
      return _.upperCase(_.get(lbu, 'postArea', '')).indexOf(area) !== -1;
   }
   
   function filterHit(lbu) {
      console.log("filterHit: ",lbu.name, (!formParams.webstore), isPostArea(lbu, 'BROMMA'), lbu.postArea ,  formParams.webstore, lbu.webstore);
      if (!lbu) return false;
      return (
         ( (!formParams.webstore) && isPostArea(lbu, 'BROMMA')) ||
            ( formParams.webstore && lbu.webstore)
      );      
   }


   function getMatchingFields(hit) {
      //      return _.keys(_.get(hit,'highlight', {}));
      return _.get(hit,'highlight', {});
   }

   function getHits(asyncResult) {
      return _.get(asyncResult, '[1].hits.hits', []);
   }

   function getLbuId(hit) {
      return _.get(hit, '_source.lbu', '');
   }


   function renderSearchResult(searchResult) {
      resultsPlaceholder.innerHTML = template({
         formParams: formParams,
         lbus: searchResult,
         error: searchResult.length === 0
      });
   }

   var googleMap;
   var markers = [];
   var myCenter = new google.maps.LatLng(59.355454, 17.945266);

   function renderMapSearchResult(searchResult) {
      // console.log("RENDER MAP");
      if (googleMap) {
         google.maps.event.trigger(googleMap,'resize');
         googleMap.setZoom( googleMap.getZoom() );
      }
      if (searchResult && searchResult[0]) {
         _.forEach(markers, function (marker) {
            marker.setMap(null);
         });
         markers = [];
      }
      var i = 0;
      _.forEach(searchResult, function (lbu) {
         var location = _.get(lbu, 'lbu.location', {});
         i++;
         if (i < 100) {
            var icon = '/img/markers/marker' + i + '.png';
            var title = '';
            var infowindow;
            if (lbu && lbu.lbu) {
               title = lbu.lbu.name;
               contentString = getInfoWindowText(lbu.lbu, i);
               infowindow = new google.maps.InfoWindow({
                  content: contentString,
                  maxWidth: 240
               });
            }
            var marker = new google.maps.Marker({
               position: new google.maps.LatLng(location.lat, location.lon),
               map: googleMap,
               title: title,
               icon: icon
            });

            if (infowindow) {
               marker.addListener('click', function () {
                  infowindow.open(googleMap, marker)
               });
               markers.push(marker);
            }
         }
      });
      addRedMarker();
   }

   function renderMapLbu(cid, i) {
      var slbu = lbuMap[cid];
      if (markers) {
         _.forEach(markers, function (marker) {
            marker.setMap(null);
         });
         markers = [];
      }
      if (googleMap) {
         google.maps.event.trigger(googleMap,'resize');
         googleMap.setZoom( googleMap.getZoom() );
      }
      if (slbu) {
         var location = _.get(slbu, 'location', {});
         var nr = i + 1;
         var icon = '/img/markers/marker' + (nr < 100 ? nr : '') + '.png';
         var title = '';
         title = slbu.name;

         contentString = getInfoWindowText(slbu, nr);
         var infowindow = new google.maps.InfoWindow({
            content: contentString
         });
         var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.lat, location.lon),
            map: googleMap,
            title: title,
            icon: icon
         });

         if (infowindow) {
            marker.addListener('click', function () {
               infowindow.open(googleMap, marker)
            });
            markers.push(marker);
         }
      }
      formParams.lbucid = undefined;
      addRedMarker();
   }

   function addRedMarker() {
      // Add your red pos marker
      marker = new google.maps.Marker({
         position: myCenter,
         map: googleMap,
         draggable: true,
         title: 'You are here!',
         icon: '/img/markers/red.png',
         animation: google.maps.Animation.DROP
      });
      contentString = `<div id="content">
          <div id="siteNotice">
          </div>
          <h4 id="firstHeading" class="firstHeading">You are here!</h4>
       </div>`;
      infowindow = new google.maps.InfoWindow({
         content: contentString
      });
      marker.addListener('click', function () {
         infowindow.open(googleMap, marker);
      });
      markers.push(marker);
      wasMap = nowMap;
   }

   function getInfoWindowText(lbu, i) {

      var logoUrl = lbu.logoUrl ? `<img src="${lbu.logoUrl}" class="card-media zmap-img" onerror="this.style.display='none'">` : '';
      var deepLink = '';
      if (lbu.deepLink.phrase) {
         deepLink = `<div class="zmap-medium"><a class="map-store-blue zmap-medium" href="${lbu.deepLink.lbuUrl}" target="blank">${lbu.deepLink.lbuName} is selling
          ${lbu.deepLink.phrase}</a><br/>
          <a class="map-store-green zmap-small" href="${lbu.deepLink.lbuUrl}" target="blank">${lbu.deepLink.displayUrl}</a></div>`;
      } else if (lbu.deepLink.articleName) {
         deepLink = `<div class="store-blue zmap-medium" cid="${lbu.cid}">
         <a href="${lbu.deepLink.articleUrl}"  class="store-blue zmap-medium" target="_blank">${lbu.deepLink.articleName}</a>
         <br/>
         <a href="${lbu.deepLink.articleUrl}"  class="map-store-green zmap-small" target="_blank">${trimUrl(lbu.deepLink.articleUrl)}</a>
         </div>`;
      } else if (lbu.deepLink.url) {
         deepLink = `<a class="map-store-green zmap-small" href="${lbu.deepLink.url}" target="blank">${trimUrl(lbu.deepLink.url)}</a>`;
      }

      return `
         <div class="nd2-card2 card-media-right card-media-small">
            <div class="card-media zmap-image">
            </div>
            <div class="card-title has-supporting-text postdist">
                <h3 class="card-primary-title zmap-general zmap-big">${i}. ${lbu.name}
                </h3>
                <a href="tel:${lbu.phone}" class="zmap-general zmap-medium">${lbu.phone}</a>
                <br/>
                <div class="zmap-general zmap-medium">${lbu.street}<br/>${lbu.postArea}</div>
            </div>
            <div class="card-supporting-text has-action has-title">
               ${deepLink}
            </div>
         </div>
        `;
   }


   var lbuapi = "/api/scorelab4/lbu/"; // http://192.168.1.6:4000/api/scorelab4/lbu/3129055025433572598
                                       // http://192.168.1.5:4000/api/scorelab4/lbu/1411408681542/articles/Backventil

   var lbuTemplateScript = $("#lbu-template").html();
   var lbuTemplate = Handlebars.compile(lbuTemplateScript);
   window.setLbu = function (cid) {
      $.getJSON(lbuapi + cid, {})
          .done(function (data) {
             $(".lbuNav").html(lbuTemplate({
                lbu: data
             }));
          });
   };

   var lbuarticlesTemplateScript = $("#lbuarticles-template").html();
   var lbuarticlesTemplate = Handlebars.compile(lbuarticlesTemplateScript);
   window.setLbuarticles = function (cid, q) {
      $.getJSON(lbuapi + cid + '/articles/' + q, {})
          .done(function (data) {
             $(".lbuarticlesNav").html(lbuarticlesTemplate({
                lbuarticle: data
             }));
          });
   };

   var getopposite = {
      'webstore': 'nearme',
      'nearme': 'webstore'
   };
   setCheckBox = function (inputclass, label) {
      formParams[inputclass] = !formParams[inputclass];
      localStorage.setItem('shmapz.'+inputclass, formParams[inputclass]);

      if (formParams[inputclass]) {
         formParams[getopposite[inputclass]] = false;
         var oppositelabel = label.closest('.ui-checkbox').siblings('div').children('label');
         localStorage.setItem('shmapz.'+getopposite[inputclass], false);
         oppositelabel.removeClass('ui-btn-active ui-checkbox-on');
         oppositelabel.addClass('ui-checkbox-off');
         label.addClass('ui-btn-active ui-checkbox-on');
         label.removeClass('ui-checkbox-off');
      } else {
         label.removeClass('ui-btn-active ui-checkbox-on');
         label.addClass('ui-checkbox-off');
      }
   };

   setCheckBoxes = function () {
      _.forIn(getopposite, function (value, inputclass) {
         var label = $('.' + inputclass);
         localStorage.setItem('shmapz.'+inputclass, formParams[inputclass]);
         if (formParams[inputclass]) {
            label.removeClass('ui-checkbox-off');
            label.addClass('ui-btn-active ui-checkbox-on');
         } else {
            label.removeClass('ui-btn-active ui-checkbox-on');
            label.addClass('ui-checkbox-off');
         }
      });
   };


   function setSearchVal(s) {
      if (s === null) {
         localStorage.setItem('shmapz.q', '');
         formParams.q = '';
         $('.search-field').val('');
      } else {
         formParams.q = s || localStorage.getItem('shmapz.q');
         if (formParams.q) {
            localStorage.setItem('shmapz.q', formParams.q);
            $('.search-field').val(formParams.q);
         }
      }
      storeCheckBoxValues();
   }

   function storeCheckBoxValues() {
      //console.log('formParams',formParams);
      if (formParams.q) {
         localStorage.setItem('shmapz.q', formParams.q);
      }
      if (formParams.nearme) {
         localStorage.setItem('shmapz.nearme', formParams.nearme);
      }
      if (formParams.webstore) {
         localStorage.setItem('shmapz.webstore', formParams.webstore);
      }
   }

   $(document).on('pagebeforeshow', '#first-view', function(){
      googleMap = null;
      setSearchVal(null);
      formParams.nearme = false;
      formParams.webstore = false;
      storeCheckBoxValues();
   });

   $(window).on('load', function () {
      var hash = window.location.hash;
      $.mobile.loading("show", {textVisible: false, html: '<div class="z-loader"><img src="/img/ring-deep-purple.gif"/></div>'});
      clock('', 'start');
      asyncQuery(apiUrl + 'lbu', {parameterMap: {q: '*'}})(function (error, lbuSearchResult) {
         _(_.get(lbuSearchResult, 'body.hits.hits', []))
             .forEach(function (lbuHit) {
                var lbu = lbuHit._source;
                lbuMap[lbu.cid] = lbu;
             });
         $.mobile.loading( "hide" );
         clock('Init load LBU: ' + Object.keys(lbuMap).length + ' items, ', 'stop');
         //console.log("LOADED EXAMPLE: " + JSON.stringify(lbuMap['1411224688433'], null, 4));
         // Now we can search :-)
         if (hash && formParams.q) {
            if (hash === '#list-view') {
               return search();
            } else if (hash === '#lbu-view') {
               window.location.hash = '#list-view';
               return search();
            } else if (hash === '#map-view') {
               return search();
            }
         }
      });

   });

   function detectBrowser() {
      var useragent = navigator.userAgent;
      var mapdiv = document.getElementById("google-map");
      if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 ) {
         mapdiv.style.width = '100%';
         mapdiv.style.height = '450px';
      } else {
         mapdiv.style.width = '100%';
         mapdiv.style.height = '768px';
      }
   }
   detectBrowser();

   function loadMap(page) {
      var canvas = $("#google-map", page)[0],
          mapOptions = {
             center: myCenter,
             zoom: 12,
             streetViewControl: false
          };

      nowMap = formParams.lbucid + formParams.q + formParams.nearme + formParams.webstore;

      if (wasMap === nowMap) {
         if (googleMap) {
            google.maps.event.trigger(googleMap, 'resize');
            //googleMap.setZoom( googleMap.getZoom() );
         }
      } else if (formParams.lbucid) {
         if (!googleMap) {
            googleMap = new google.maps.Map(canvas, mapOptions);
         }
         renderMapLbu(formParams.lbucid, formParams.lbupos);
      } else if (!googleMap) {
         googleMap = new google.maps.Map(canvas, mapOptions);
         setSearchVal(formParams.q);
         if (formParams.q) {
            search();
         } else {
            addRedMarker();
         }
      } else {
         setSearchVal(formParams.q);
         if (formParams.q) {
            renderMapSearchResult(window.hitList);
         } else {
            addRedMarker();
         }
      }
   }

   $('.search-form').on('keypress', function (event) {
      'use strict';
      if (event && event.keyCode == 13) {
         event.preventDefault();
         setSearchVal(document.activeElement.value);
         document.activeElement.blur();
         $.mobile.changePage("#list-view");
         var parent = $(this).closest('.ui-page-active');
         formParams.lbucid = undefined;
         return search();
      }
   });

   $('.search-form-map').on('keypress', function (event) {
      'use strict';
      if (event && event.keyCode == 13) {
         event.preventDefault();
         setSearchVal(document.activeElement.value);
         document.activeElement.blur();
         return search();
      }
   });


   $(document).on("pagecontainershow", function (e, data) {
      formParams.lang = getLang();
      if (formParams.lang) {
         localStorage.setItem('shmapz.lang', formParams.lang);
      }

      $('div.footer').html(footerTemplate());
      $('div.fieldset').html(fieldSetTemplate());

      $('.nearme').on('click', function (event) {
         'use strict';
         event.preventDefault();
         //console.log("THIS: ", $(this));
         setSearchVal($(this).closest('.search-form').find('.search-field').val());
         setCheckBox('nearme',$(this));
         if ($.mobile.activePage.attr('id') !== 'list-view')
            $.mobile.changePage("#list-view");
         return search();
      });

      $('.webstore').on('click', function (event) {
         event.preventDefault();
         setSearchVal($(this).closest('.search-form').find('.search-field').val());
         setCheckBox('webstore',$(this));
         if ($.mobile.activePage.attr('id') !== 'list-view')
            $.mobile.changePage("#list-view");
         return search();
      });

      setCheckBoxes();
      setSearchVal(formParams.q);
      if (data.toPage[0].id == "map-view") {
         loadMap(data.toPage);
      }
   });


   Handlebars.registerHelper('inc', function (value, options) {
      return parseInt(value) + 1;
   });

   Handlebars.registerHelper('explainPopup', function() {
      var explainContent = (this.lbuHints) ? JSON.stringify(this.lbuHints,null,2) : '';
      return new Handlebars.SafeString(explainContent);
   });


   Handlebars.registerHelper('distance', function (lbu) {
      if (!lbu || !lbu.distance || !formParams.nearme) {
         return "";
      }
      var d = lbu.distance;
      var result = (d > 1) ? Math.round(d * 10) / 10 + ' km' : Math.round(d * 100) * 10 + ' m';
      return new Handlebars.SafeString(result);
   });


   function isArticle(searchHit) {
      var match = _.get(searchHit, 'matchingFields', {});
      if ((match.name) || (match['phrases.value'])) {
         return false;
      } else {
         return (searchHit.articleHit !=  undefined);
      }
   }

   function addDeepLink(searchHit) {
      var lbu = searchHit.lbu;
      var url = _.get(lbu, 'url', "");
      if (url.indexOf('missing') > -1) url = '';
      var displayUrl = trimUrl(url);
      var s = '';
      var deepLink = {};
      if (!lbu) return new Handlebars.SafeString(s);
      var match = _.get(searchHit, 'matchingFields', {});
      if ((match.name) || (match['phrases.value'])) {
         deepLink = {url: url, displayUrl: displayUrl};
      } else if (searchHit.articleHit) {
         displayUrl = trimUrl(searchHit.articleHit.url);
         deepLink = {articleName: searchHit.articleHit.articleName, articleUrl: searchHit.articleHit.url, displayUrl: displayUrl};
      } else if ((match['phrases.brand']) || (match['phrases.product']) || (match['categoryProducts']) || (match['categoryNames'])) {
         var m = _(match).values().flattenDeep().value();
         var p = ( _.isEmpty(m)) ? '' : m[0];
         deepLink = {lbuName: lbu.name, phrase: p, lbuUrl: url, displayUrl: displayUrl};
      }
      lbu.deepLink = deepLink;
   }
   
   Handlebars.registerHelper('deepLink', function(cid, searchHit) {
      var lbu = searchHit.lbu;
      var url = _.get(lbu, 'url', "");
      if (url.indexOf('missing') > -1) url='';
      var displayUrl = trimUrl(url);
      var s = '';
      if (!lbu) return new Handlebars.SafeString(s);
      var match = _.get(searchHit, 'matchingFields', {});

      if ((match.name) || (match['phrases.value'])) {
         s = '<a  class="store-green" href="'+url+'" target="blank">'+displayUrl+'</a>';
      }
      else if (searchHit.articleHit) {
         s = '<div class="store-blue" cid="'+lbu.cid+'">'+
            '<a class="store-blue" href="'+searchHit.articleHit.url+'" class="z-article" target="_blank">'+searchHit.articleHit.articleName+'</a>'+
            '<br/>'+
            '<a class="store-green" href="'+searchHit.articleHit.url+'" class="z-url" target="_blank">'+trimUrl(searchHit.articleHit.url)+'</a>'+
            '</div>';
      }
      else if ((match['phrases.brand']) || (match['phrases.product']) || (match['categoryProducts'])  || (match['categoryNames'])) {
         var m = _(match)
                .values()
                .flattenDeep()
                .value();
         var p = ( _.isEmpty(m)) ? '' : m[0];
         var plainPhrase = p.replace(/<[^>]+>/g, '');

         s = '<div class="store-blue"><a class="store-blue" href="' + url + '" target="_blank">' + lbu.name +
             ' is selling ' + p + '</a><br/><a class="store-green" href="' + url + '" target="_blank">' + displayUrl + '</a></div>';
      }
      return new Handlebars.SafeString(s);

   });

   Handlebars.registerHelper('articleImage', function(searchHit) {            
      var s =
          (_.get(searchHit, ['articleHit', 'imageUrl'])) && searchHit.articleHit.url !== 'missing' ?
             '<a href="'+searchHit.articleHit.url+'" target="_blank">'+
             '<img src="'+searchHit.articleHit.imageUrl+'" onerror="this.style.display=\'none\'"/>'+
             '</a>' :
             '';
      if (!isArticle(searchHit)) s = '';
      return new Handlebars.SafeString(s);
      
   });

   var fieldSetTemplate = Handlebars.compile($("#fieldset").html().toString());
   Handlebars.registerHelper('fieldset', function() {
      var fieldset =  `<fieldset data-role="controlgroup" class="search-buttons ui-controlgroup ui-controlgroup-horizontal ui-corner-all" data-type="horizontal" data-mini="false">
         <div class="ui-controlgroup-controls ">
            <div class="ui-checkbox">
               <label class="clr-btn-accent-deep-purple waves-effect ui-btn ui-corner-all ui-btn-inherit ui-first-child nearme">Near me</label>
            </div>
            <div class="ui-checkbox">
               <label class="clr-btn-accent-deep-purple  waves-effect ui-btn ui-corner-all ui-btn-inherit ui-last-child webstore">Only webshops</label>
            </div>
         </div>
      </fieldset>`;
      return new Handlebars.SafeString(fieldset);
   });

   var footerTemplate = Handlebars.compile($("#footer").html().toString());
   Handlebars.registerHelper('footer', function () {
      var hash = window.location.hash;
      var first = true;
      var list = true;
      var map = true;

      if (hash === '' || hash === '#first-view') {
         first = false;
      } else if (hash === '#list-view') {
         list = false;
      } else if (hash === '#map-view') {
         map = false;
      }

      var footer = `<div data-role="footer" data-position="fixed" role="contentinfo" data-tap-toggle="false" class="ui-footer ui-bar-inherit ui-footer-fixed slideup">
         <div class="row center-xs">
            <div class="col-xs-4">
                <div class="box">
                    <a href="#first-view" class="z-color-${first} ui-btn ui-mini nd2-btn-icon-block waves-effect waves-button">
                        <i class="zmdi zmdi-info"></i>
                        Store Hunters
                    </a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <a href="#list-view" class="z-color-${list} ui-btn ui-mini nd2-btn-icon-block waves-effect waves-button">
                        <i class="zmdi zmdi-view-list"></i>
                        List view
                    </a>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="box">
                    <a href="#map-view" class="z-color-${map} ui-btn ui-mini nd2-btn-icon-block waves-effect waves-button">
                        <i class="zmdi zmdi-map"></i>
                        Map view
                    </a>
                </div>
            </div>
         </div>
      </div>`;
      return new Handlebars.SafeString(footer);
   });

   Handlebars.registerHelper('articlelist', function (context, options) {
      var ret = `<br/><ul data-role="listview" data-icon="false" class="ui-listview">
                     <li data-role="list-divider" role="heading" class="ui-li-divider ui-bar-inherit ui-first-child">
                        ${context.length ? 'Products' : ''}
                     </li>`;
      _.forEach(context, function (item) {
         ret += '<li class="ui-li-has-thumb">' + options.fn(item) + '</li>';
      });
      return ret + '</ul>';
   });
   
   function calculateDistance(lbu) {
      if (!lbu || !lbu.location || !formParams.nearme) {
         return undefined;
      } else {
         return distance(
            lbu.location.lat,
            lbu.location.lon,
            myCenter.lat(),
            myCenter.lng()
         );
      }
   }

   function distance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;    // Math.PI / 180
      var c = Math.cos;
      var a = 0.5 - c((lat2 - lat1) * p)/2 + 
             c(lat1 * p) * c(lat2 * p) * 
             (1 - c((lon2 - lon1) * p))/2;
      
      return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
   }

   exports.clock=clock;
}());
