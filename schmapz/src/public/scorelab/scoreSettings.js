

(function() {

   var Handlebars=require('handlebars');
   var _=require('lodash');
   
   var scoreFields = {
      general: {
         "showExplain": 0
      },
      articleFields: {
         "url" : 1,
         "articleName" : 10,
         "breadCrumb" : 1,
         "priowords" : 1
      },
      collectionFields: {
         "url" : 1,
         "articleName" : 10,
         "breadCrumb" : 1,
         "textEx" : 1
      },
      lbuFields : {
         "name" : 10,
         "category" : 1,
         "categoryProducts" : 1,
         "categoryNames" : 1,
         "phrases.product" : 1,
         "phrases.brand" : 1,
         "phrases.geo" : 1,
         "phrases.value" : 1,
         "article" : 1  
      }
   };


   function renderScoreFields() {
      var source = document.getElementById('parameters-template').innerHTML;
      var template = Handlebars.compile(source);
      var result = template({scoreFields:scoreFields});
      var resultsPlaceholder = document.getElementById('parameters-out');
      resultsPlaceholder.innerHTML = result;
   }

   
   function getScoreSettings() {      
      for(var block in scoreFields) {
         for(var field in scoreFields[block]) {
            var elementId = block+"."+field;
            var element = document.getElementById(elementId);
            if(element)            
               scoreFields[block][field] = element.value;
         }
      }
      return scoreFields;
};
  
   renderScoreFields();

   exports.getScoreSettings=getScoreSettings;
   
}());




