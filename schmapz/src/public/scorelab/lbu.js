/**
 * Created by mla on 16-04-26.
 */


(function () {

    var Handlebars = require('handlebars');
    var lbuapi = "/api/scorelab4/lbu/"; // http://192.168.1.6:4000/api/scorelab4/lbu/3129055025433572598

    $(function () {
        // Get the HTML from lbu template in the script tag​
        var lbuTemplateScript = $("#lbu-template").html();

        // Compile the template​
        var lbuTemplate = Handlebars.compile(lbuTemplateScript);

        window.setLbu = function (cid) {
            //console.log('cid:' + cid);
            $.getJSON(lbuapi + cid, {}
            ).done(function (data) {
                    $(".lbuNav").html(lbuTemplate({
                        lbu: data
                    }));
                });
        };
    });

}());
