var geodata = { "type": "FeatureCollection",
    "features": [
      { "type": "Feature",
        "geometry": {"type": "Point", "coordinates": [18.0669000,59.3355800]},
        "properties": {"description": "Sunfleet car pool"}
        },
      { "type": "Feature",
        "geometry": {"type": "Point", "coordinates": [18.0679000,59.3455800]},
        "properties": {"description": "<h1>Pizza</h1>Really tasty pizza!"}
        },
      { "type": "Feature",
        "geometry": {"type": "Point", "coordinates": [18.0609000,59.3399800]},
        "properties": {"description": "<h2>Hamburgers</h2>Best burgers in town"}
        }
    ]
};
