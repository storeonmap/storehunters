#!/bin/bash

create_heroku_dev_app() {
    heroku apps:create flowsimdev01 --remote flowsimdev
}

add_existing_heroku_dev_app() {
    git remote add flowsimdev https://git.heroku.com/flowsimdev01.git
    git remote -v
}

deploy_heroku_dev() {
    git push flowsimdev develop:master
}

watchify_start() {
    watchify public/scorelab/main.js -o public/scorelab/bundle.js -dv   
}

# NOTE: This is used by heroku via package.json:postinstall
bundlejs() {
    ./node_modules/browserify/bin/cmd.js src/public/scorelab/main.js > src/public/scorelab/bundle.js
}

if [[ -z "$1" ]]; then
   echo "usage:"
   echo "./manage.sh create_heroku_dev_app"
   echo "./manage.sh deploy_heroku_dev"
   echo "./manage.sh watchify_start"
   echo "./manage.sh bundlejs"
   echo "./manage.sh add_existing_heroku_dev_app"
fi

$*

