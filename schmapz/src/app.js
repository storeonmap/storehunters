var express = require('express');
var path = require('path');
var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');

var BasicStrategy = require('passport-http').BasicStrategy;
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


var lbu = require('./routes/lbu/lbu2');
var stemming = require('./routes/stemming/stemming01');
var distancesuggest = require('./routes/distancesuggest/distancesuggest');
var autocomplete = require('./routes/autocomplete/autocomplete');
var scorelab2 = require('./routes/scorelab/scorelab2');
var scorelab3 = require('./routes/scorelab/scorelab3');

var query4 = require('./routes/scorelab/scorelab4/query');
var article = require('./routes/scorelab/scorelab4/article');  // TODO: Rename - mala


app.set('port', process.env.SCHMAPZ_PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/bower_components', express.static('bower_components'));
app.use('/node_modules', express.static('node_modules'));
app.use(express.static('public'));
app.use(logger('dev'));

app.use(cookieParser());
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); 
app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash());

passport.use(new LocalStrategy(
    function(username, password, done) {
       console.log("Auth: LocalStrategy");
        if (username == "bob" && password == "sh0pp3r")
           return done(null, {});
        else
            return done(null, false);
    }
));

var isAuthenticated = function (req, res, next) {
   if (!req.isAuthenticated()) {
      res.redirect('/login');
   }
   return next();  
}

app.post('/login',
         passport.authenticate('local', { successRedirect: '/shmapz',
                                          failureRedirect: '/login',
                                          failureFlash: true })
        );

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

app.get('/login', function(req, res) {
   res.sendFile(path.join(__dirname, 'public','login.html'));
});




app.get('/', isAuthenticated, function(req, res) {   
   res.redirect('/shmapz');
});

app.get('/shmapz/:lang?', isAuthenticated, function(req, res) { // Optional :lang? is extracted by browser-side script 
    res.sendFile(path.join(__dirname,'protected','index2.html'));
});





//--- Latest ----
app.post('/api/scorelab4/api/v4/lbu/search/article',  query4.searchLbuByArticle);
app.post('/api/scorelab4/api/v4/lbu/search/collection',  query4.searchLbuByCollection);
app.post('/api/scorelab4/api/v4/lbu/search/lbu',  query4.searchLbu);

//app.get('/api/scorelab4/article/:id', article.getArticle);
//app.get('/api/scorelab4/articles/:lbu', article.getArticles);
//app.get('/api/scorelab4/article-search/:lbu', article.getArticles);
app.get('/api/scorelab4/lbu/:id/articles/:q', article.articlesSearch);
app.get('/api/scorelab4/lbu/:id', article.getLbu);  // TODO: Rename - mala

//--- END Latest ----




//--- Labs ---
app.get('/api/list/article', lbu.articleSearchAsList)
app.post('/api/scorelab2/article', scorelab2.articleSearchAsList)
app.post('/api/scorelab3/article', scorelab3.articleSearchAsList)
app.get('/api/scorelab3/article', scorelab3.articleSearchAsList)
app.get('/api/stemming/stemming01', stemming.stemmingQuery)
app.get('/api/autocomplete', autocomplete.autocomplete)
//--- Labs ---



io.on('connection',
      function(socket){
          distancesuggest.socketStart(socket, io)
      });


http.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


