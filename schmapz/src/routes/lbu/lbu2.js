var async = require('async');
var elasticsearch = require('elasticsearch');
var hl = require('highland');
var _ = require('underscore');

var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
  //,log: 'trace'
});



exports.articleSearchAsList = function(req, res) {
    var q = req.query.q;
    console.log("Query="+q)
    doWaterfall(q, res);   
};



var doWaterfall = function(q,res) {
    var resultMap={}
    async.waterfall([
        _.partial(search1, q, resultMap),
        filterResponse1,
        search2,
        filterResponse2,
        _.partial(renderResult, res),
    ], errorHandler );
}


query1 = function(q) {
    andQuery = 
        (''+q).trim()
        .split(/\s+/)
        .map(function(x){return "*"+x+"*"})
        .join(' AND ') //q.replace("/[ ]*/g", " AND ")
    console.log(andQuery)
    return {
        index: 'lbu',
        "size":4000,
        body: {
            "query": {
                "query_string" : {
                    "default_field" : "_all",
                    "query" : andQuery
                }
            }
        }
    }
}

                        
 

lbuQuery2 = function(lbuList) {
    return {
        index: 'lbu',
        type:'lbu',
        size:4000,
        body: {
            filter : {
                terms : {
                    cid : lbuList
                }
            }
        }
    };
}



var getLbuId = function(resultMap, hit){
    var lbuId = hit._source.cid
    if (hit._type == "article") {
        //console.log(hit)
        lbuId = hit._source.lbu
    }
    resultMap[lbuId] = hit._source.url
    return lbuId        
}


var chainedSearch = function(query, resultMap, callback) {
    client.search(query,     
                  function(err, response, status) {
                      if(err)
                          callback(err)
                      else
                          callback(null, response, resultMap)
                  });
}


var search1 = function(q,resultMap, callback) { chainedSearch(query1(q), resultMap, callback) }


var filterResponse1 = function( response, resultMap, callback) {
    var a=response.hits.hits;
    hl(a)
        .map(_.partial(getLbuId, resultMap))
        .uniq()
        .collect()
        .apply(function (lbuList) {
            callback(null, lbuList, resultMap)
        });    
}

var search2 = function(lbuList, resultMap, callback) { chainedSearch(lbuQuery2(lbuList), resultMap, callback) }

var filterResponse2 = function(response, resultMap, callback) {
    var hits=response.hits.hits;
    hits.forEach(function (hit) {        
        var lbuId = hit._source.cid
        hit._source.showUrl=resultMap[lbuId]
        resultMap[lbuId] = hit._source
    });
    callback(null,resultMap)
}


var toSearchListJson = function(resultMap) {
    res=[]
    for (var key in resultMap) {
        res.push(resultMap[key])
    };
    var listJson={result:res}
    return listJson;
}


var renderResult = function(res, resultMap, callback) {
    res.json(toSearchListJson(resultMap));
    callback()
}

var errorHandler = function (err, result) {
    if(err) console.trace(err);
}



