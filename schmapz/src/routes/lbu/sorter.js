var SortedArray = require("collections/sorted-array");

exports.sortMapArrayOnField = function(field,array) {
    return result = new SortedArray.SortedArray(array,
        function(a,b) {
            return normalizeSwedishCharacters(a[field]) === normalizeSwedishCharacters(b[field])
        },
        function(a,b) {
            return normalizeSwedishCharacters(a[field]).localeCompare(normalizeSwedishCharacters(b[field]))
        }
    ).toArray();
}

exports.sortArray = function(array) {
    return result = new SortedArray.SortedArray(array,
        function(a,b) {
            return normalizeSwedishCharacters(a) === normalizeSwedishCharacters(b)
        },
        function(a,b) {
            return normalizeSwedishCharacters(a).localeCompare(normalizeSwedishCharacters(b))
        }
    ).toArray();
}

exports.sortArrayUniq = function(array) {
    var a = exports.sortArray(array).map(function(n) {return n.trim()});
    return a.filter(function(item, pos) {
        return !item.match(/^[‧  ]*$/) && (!pos || normalizeSwedishCharacters(item) !== normalizeSwedishCharacters(a[pos - 1]));
    })
    return a;
}

exports.sortCommaSeparated = function(s) { // TODO: Remove this function
    return exports.sortArray(s.split(/, */)).join(', ').replace(/ och /ig," & ")
}

// nodejs does not handle swedish characters sort order :-(

var normalizeSwedishCharacters = function(s) {
    return !s ? "" : s.toLocaleLowerCase().
        replace(/[áà]/g, "a").
        replace(/[éèëê]/g, "e").
        replace(/ü/g, "u").
        replace(/ø/g, "ö").
        replace(/°/g, "").
        replace(/å/g, "°").
        replace(/[äæ]/g, "å").
        replace(/°/g, "ä");
}

String.prototype.capitalize=function(all){
    if(all){
        return this.split(' ').map(function(e){return e.capitalize();}).join(' ');
    }else{
        return this.length > 1 ? this.charAt(0).toLocaleUpperCase() + this.slice(1).toLocaleLowerCase() : this;
    }
}

String.prototype.zEncode = function() {
    return this.replace(/&/g,"__och__");
}

String.prototype.zDecode = function() {
    return this.replace(/__och__/g,"&");
}