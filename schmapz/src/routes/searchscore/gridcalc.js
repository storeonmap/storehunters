
var elasticsearch = require('elasticsearch');
var hl = require('highland');
var async = require('async');
var _ = require('underscore');

var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
});

//var c = hl.streamifyAll(client);




var tt = function(indata, callback){
    callback(null,  "hej "+indata)
}

var wrapT = hl.wrapCallback(tt);

exports.test = function(a) {
    wrapT("foo")
        .each(hl.log);
}



exports.query = function(q) {
    wrappedQuery( elasticQuery1 ) 
        .each(hl.log);
}


var wrappedQuery = hl.wrapCallback(normalQuery);

var normalQuery=function(q, callback) {
    doElasticQuery(elasticQuery1, callback);
}

var doElasticQuery=function(query, callback) {
    console.log('doQuery: '+query)
    client.search(query,     
                  function(err, response, status) {
                      console.log("resp: "+response);
                      if(err)
                          callback(err)
                      else
                          callback(null, response)
                  });
}







var splitQuery = function(q) {
    return q.trim()
        .split(/\s+/)
        .map(function(x){return "*"+x+"*"})
        .join(' AND ') //q.replace("/[ ]*/g", " AND ")
}

var elasticQuery1 = {
    index: 'lbu',
    "size":4000,
    body: {
        "query": {
            "query_string" : {
                "default_field" : "_all",
                "query" : "ica"
            }
        }
    }
}



exports.calcWaterfall = function(indata, listOfFunctions, outerCallback) {
    var fn = [
        _.partial(listOfFunctions[0], indata),
        listOfFunctions[1],
        function(indata, callback) {
            console.log(">>> "+indata)
            outerCallback(null, indata)
            callback(null, indata)
        }
    ]
    async.waterfall(fn)
}
