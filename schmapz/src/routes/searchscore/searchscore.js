
var elasticsearch = require('elasticsearch');
var hl = require('highland');
var async = require('async');
var _ = require('underscore');

var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
});




exports.search = function(req,res) {

    var searchFunctions = [queryWaterfall1, queryWaterfall2]
    var renderResultCallback = _.partial(renderResult, res)
    
    async.parallel(
        searchFunctions,
        renderResultCallback
    );
}


var queryWaterfall1 = function(parallellCallback) {
    async.waterfall([
        query1,
        _.partial(calc1, parallellCallback)
    ], errorHandler );
}


var query1 = function(waterfallCallback1) {
    waterfallCallback1(null,'hopp')
}


var calc1 = function(parallellCallback, searchResult, waterfallCallback2) {
    var calcres = "calc>"+searchResult
    parallellCallback(null, calcres)
    waterfallCallback2(null,calcres)
}



var queryWaterfall2 = function(parallellCallback) {
    async.waterfall([
        query2,
        _.partial(calc1, parallellCallback)
    ], errorHandler );
}



var errorHandler = function (err, result) {
    if(err) console.trace(err);
}


var query2 = function(callback) {
    callback(null,'hej')
}



var renderResult = function(httpResult, err,data) {
    httpResult.json(data)
}





