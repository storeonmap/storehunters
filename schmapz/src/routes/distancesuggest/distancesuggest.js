

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'localhost:9200',
//  log: 'trace'
});



var geoAgg = {
    "aggregations" : {
        "rings_around" : {
            "geo_distance" : {
                "field" : "location",
                "origin" : "59.355532, 17.956194",
                "unit": "m",
                "ranges" : [
                    { "to" : 500 },
                    { "from" : 500, "to" : 1000 },
                    { "from" : 1000, "to" : 2000 },
                    { "from" : 2000 },
                ]
            },
            "aggregations" : {
                "mycat": {
                    "terms": {
                        "field": "category"
                    }
                }
            }
        }
    }
}

elasticQuery = function(q) {
    return {
        index:"cats",
        search_type: "count",
        q: "*"+q+"*",
        body: geoAgg        
    }
}


exports.socketStart = function(socket, io){
    socket.on('chat message', function(msg){
        if(!msg) msg='EMPTY_SEARCH'
        client.search(
            elasticQuery(msg)
        ).then(function (body) {
            io.emit('chat message', JSON.stringify(body.aggregations.rings_around.buckets));          
        }, function (error) {
            console.trace(error.message);
        });                    
    });
}

