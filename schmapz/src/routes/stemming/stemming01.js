var async = require('async');
var elasticsearch = require('elasticsearch');
var hl = require('highland');
var _ = require('underscore');

var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
  //,log: 'trace'
});



exports.stemmingQuery = function(req, res) {
    var q = req.query.q;
    console.log("Query="+q)
        
    async.map([noStemmingQuery(q), stemmingQuery(q)], doSearch, function(err, results){
        renderResult(res, results);
    });
};


stemmingQuery = function(q) {
    return {
        index: 'stemming01',
        type:'stemming01',
        size:4000,
        body: {
            "query": {
                "query_string" : {
                    "default_field" : "category",
                    "query" : q
                }
            }
        }
    };
}

noStemmingQuery = function(q) {
    return {
        index: 'stemming01',
        type:'stemming01',
        size:4000,
        body: {
            query: {
                multi_match: {
                    type:     "most_fields", 
                    query:    q,
                    fields: ["category" ]
                }
            }
        }
    };
}



doSearch = function(query, callback) {
    client.search(query,     
                  function(err, response, status) {
                      if(err)
                          callback(err, response)
                      else
                          callback(null, filterQueryResult(response))
                  });
}


filterQueryResult = function(queryResult) {
    return  _.uniq(_.map(queryResult.hits.hits, function(hit) {return hit._source.category}));
}
    

var toSearchListJson = function(resultMap) {
    var listJson={result:resultMap}
    console.log(listJson);
    return listJson;
}


var renderResult = function(res, resultMap) {
    res.json(toSearchListJson(resultMap));
}



