var hl = require('highland');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
  //,log: 'trace'
});




exports.autocomplete = function(req, res) {
    if (req.query.reload) {
        console.log("delete index");
        client.indices.delete(
            {index: 'autocomplete'},
            function(err, elasticResponse, status) {
                client.indices.create(
                    {index:'autocomplete'},
                    function(err, elasticResponse, status) {
                        reloadAll();
                    });
            });
    }
    var q = req.query.q;
    autocompleteSearch( q, res ); 
};

var reloadAll = function() {
        reloadCategories();
        reloadLbu();
        reloadGeo();
};


var autocompleteSearch = function(q, httpRes) {
    var query = autocompleteQuery(q);
    client.search(query, function(err, elasticResponse, status) {
        if(err)
            console.log(err);
        else
            hl(elasticResponse.hits.hits)
            .map(function(x){
                return {
                    id: x._source.id,
                    name: x._source.name,
                    type: x._source.type,
                    url: "https://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
                };
            })
            .toArray(function (xs) {
                xs.unshift(
                    {
                        id: q,
                        name: q,
                        type: 'fritext',
                        url: "https://si0.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
                    }
                );
                httpRes.json(xs);
            });
    });
};


var autocompleteQuery = function(q) {
    return {
        index: 'autocomplete',
        size: 4000,
        body: {
            "query": {
                "query_string" : {
                    "default_field" : "name",
                    "query" : "*"+q+"*"
                }
            }
        }
    };
};


var reloadCategories = function() {
    client.search(queryAllCatgories(), function(err, elasticResponse, status) {
        if(err)
            console.log(err);
        else
            client.bulk({body: pushCategories(elasticResponse.hits.hits)});
    });
};



var queryAllCatgories = function() {
    return {
        index: 'categories',
        type:'categories',
        size:4000,
        body: {
        }
    };
};


var pushCategories = function(hits) {
    var resultList = [];
    hits.forEach(function (hit) {
        var id = hit._source.id;
        var idOffset=0;
        var category = hit._source.category;
        pushAutocompleteItem(id+'_'+(idOffset++), category, "Kategori", resultList);
        hit._source.synonym.forEach(function(word) {
            pushAutocompleteItem(id+'_'+(idOffset++), word, "Kategori", resultList);
        });
    });
    return resultList;
};


var uniqueNames = {};
var pushAutocompleteItem = function(id, inputName, type, resultList) {
    var name = inputName.toLowerCase();
    if (!uniqueNames[type+"."+name]) {
        uniqueNames[type+"."+name] = name;
        resultList.push( { index:  { _index: 'autocomplete', _type: 'autocomplete', _id: id } } );
        resultList.push( {id:name, name:name, type:type} );
    }
};



    
var reloadLbu = function() {
    client.search(queryAllLbu(), function(err, elasticResponse, status) {
        if(err)
            console.log(err);
        else {
            var body = pushLbuDataList(elasticResponse.hits.hits);
            console.log("push %j",body);
            client.bulk({body:body});
        }
    });
};


var queryAllLbu = function() {
    return {
        index: 'lbu',
        type:'lbu',
        size:4000,
        body: {
        }
    };
};


var pushLbuDataList = function(hits) {
    var resultList = [];
    hits.forEach(function (hit) {
        var s = hit._source;
        var id = s.id;
        var idOffset=0;
        if (s.phrases && s.phrases.brand) {
            var brand = s.phrases.brand;
            brand.forEach(function(word) {
                pushAutocompleteItem(id+'_'+(idOffset++), word, "brand", resultList);
            });            
        }
    });
    return resultList;
};

var reloadGeo = function() {
    var resultList = [];
    pushAutocompleteItem('1', "bromma", "plats", resultList);
    pushAutocompleteItem('2', "bro", "plats", resultList);
    pushAutocompleteItem('3', "bromölla", "plats", resultList);
    console.log("reload geo: %j",resultList);
    client.bulk({body: resultList});        
};
