

exports.articleQuery = function(queryString) {
    return {
        index: 'lbu',
        type: 'article',
        "size":4000,
        body: {
            "query": {
                "query_string" : {
                    "default_field" : "_all",
                    "query" :  splitQueryString(queryString)
                }
            }
        }
    };
};

exports.lbuArticlesQuery = function (queryString, cid) {
    return {
        index: 'lbu',
        type: 'article',
        "size": 4000,
        body: {
            "query": {
                "bool": {
                    "must": [
                        {"match": {"lbu": cid}},
                        {
                            "query_string" : {
                                "default_field" : "_all",
                                "query" :  splitQueryString(queryString)
                            }
                        }
                    ]
                }
            }
        }
    };
};


exports.lbuQuery = function(queryString, parameterMap, lbuList) {
    return {
        index: 'lbu',
        type: 'lbu',
        "size":4000,
        body: {
            "query":{
                "bool": {
                    "should": [
                        {
                            "query_string":{
                                "fields": defaultFields(parameterMap),
                                "query":splitQueryString(queryString)
                            }
                        },
                        {
                            "terms": {
                                "cid":  lbuList,
                                boost: parameterMap['article']        
                            }
                        }
                    ]
                }
            }
        }
    };
};



var splitQueryString = function(q) {
    return (''+q).trim()
        .split(/\s+/)
        .map(function(x){return x+"*";}) // Only star after each word
        .join(' AND ');
    //q.replace("/[ ]*/g", " AND ")
};


var defaultFields = function(parameterMap) {
    var fields = [];
    for(param in parameterMap) {
        fields.push( param+"^"+parameterMap[param] );
    }
    return fields;
};



