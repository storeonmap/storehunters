var hl = require('highland');
var query = require('./query3');
var client = new require('elasticsearch').Client();
var wrapCallback = require('highland-wrapcallback');



exports.articleSearchAsList = function (req, res) {
    
    var parameterMap = req.body;

    elasticSearchStream(query.articleQuery(parameterMap))
        .pipe(splitHits())
        .map(storeArticle(parameterMap))
        .pluck('lbu')
        .collect()
        .consume(lbuSearchStream(parameterMap))
        .pipe(splitHits())
        .map(addArticleToLbu(parameterMap))
        .collect()
        .each(renderResult(parameterMap, res));
};



function elasticSearchStream(query) {
    console.log("Query: %j",query);
    return wrapCallback(client, 'search')(query);
}

function lbuSearchStream(parameterMap) {
    return  function(err, value, push, next) {
        if(value && (value !== hl.nil)) {
            var lbuQuery = query.lbuQuery(parameterMap, value); 
            next(elasticSearchStream(lbuQuery));
        } 
        else {
            next();
        }
    };
}


function splitHits() {
    return hl.pipeline(
        hl.pluck('hits'),
        hl.pluck('hits'),
        hl.flatten(),
        hl.pluck('_source')
    );    
}    


var addArticleToLbu = hl.curry(function(parameterMap, lbuHit) {
    if(parameterMap.lbuArticle && parameterMap.lbuArticle[lbuHit.cid]) {
        lbuHit.lbuArticle = parameterMap.lbuArticle[lbuHit.cid];
    }
    return lbuHit;
});


var storeArticle = hl.curry(function(parameterMap, articleHit) {
    if(!parameterMap.lbuArticle) {
        parameterMap.lbuArticle = {};
    }
    if(!parameterMap.lbuArticle[articleHit.lbu]) {
        parameterMap.lbuArticle[articleHit.lbu]= articleHit;
    }
    return articleHit;
});

var storeExplain = hl.curry(function(parameterMap, lbuHit) {
    if ( (parameterMap.general.showExplain != 0) &&  lbuHit._explanation && lbuHit._source) {
        lbuHit._source.lbuHints = {
            general_categories:parameterMap.result_category,
            _score: lbuHit._score,
            _explanation: lbuHit._explanation
        };
    };
    return lbuHit;
});

var renderResult = hl.curry(
    function(parameterMap,  httpResult, lbuSearchResult) {
        httpResult.json(
            {
                lbus:lbuSearchResult,
                q:parameterMap.q,
                globalHints:{
                    categories:parameterMap.result_category
                }                
            }
        );
    }
);
    


