var elasticsearch = require('elasticsearch');
var query = require('./query');

var client = new elasticsearch.Client({
    host: 'elasticsearch:9200'
});

exports.articleSearchAsList = function (req, res) {
    var parameterMap = req.body,
        q = parameterMap.q,
        cid = parameterMap.cid;
    delete parameterMap.q;
    delete parameterMap.cid;
    var articles = {},
        lbuArticle = {},
        lbuArticles = [],
        lbuMap = {},
        articleCnt = {},
        result = {};
    result.metadata = {};
    result.metadata.q = q;
    result.metadata.cid = cid;

    var articleQuery = cid ? query.lbuArticlesQuery(q, cid) : query.articleQuery(q);
    console.log("article query: " + JSON.stringify(articleQuery, null, 4));
    client.search(articleQuery).then(function (searchResult) {  // Get all articles
        searchResult.hits.hits.forEach(function (hit) {
            var article = hit._source;
            if (article.lbu) { // Article
                if (article.lbu.match("^http")) {  // some lbuIds have URL instead of cid
                    article.lbu = article.lbu.replace(/.*cid=/, "").replace(/&.*/, "");
                }
                article.articleName = typeof article.articleName === "object" ? article.articleName[0] : article.articleName;
                article.textEx = article.textEx === "missing" ? undefined : article.textEx && article.textEx.length > 140 ? article.textEx.substr(0, 140).replace(/ [^ ]*$/, " ...") : article.textEx;
                articles[article.cid] = article;
                articleCnt[article.lbu] = articleCnt[article.lbu] ? articleCnt[article.lbu] + 1 : 1;
                lbuArticle[article.lbu] = lbuArticle[article.lbu] ? lbuArticle[article.lbu] : article;  // will store the first article only for each lbu
                if(cid && cid === article.lbu) {
                    delete article.breadCrumb;
                    delete article.crawl;
                    delete article.imported;
                    delete article.price;
                    delete article.type;
                    delete article.cid;
                    delete article.lbu;
                    lbuArticles.push(article);
                }
            } // Should we store LBU if LBU?
        });

        var lbuIds = [];
        if (cid) {
            lbuIds.push(cid);
        } else {
            for (var key in articleCnt) {      // Get all lbuIds
                if (key && (key != "undefined"))
                    lbuIds.push(key);
            }
        }
        console.log("lbuQuery: " + JSON.stringify(query.lbuQuery(q, parameterMap, lbuIds), null, 4));
        client.search(query.lbuQuery(q, parameterMap, lbuIds)).then(function (searchResult) {
            searchResult.hits.hits.forEach(function (hit) { // Get all lbus for lbuIds
                var lbu = hit._source;
                lbuMap[lbu.cid] = lbu;
                articleCnt[lbu.cid] = articleCnt[lbu.cid] ? articleCnt[lbu.cid] + 1 : 1;
            });
            var lbus = [];
            for (var key in lbuMap) { // Create lbus list for client
                var lbu = lbuMap[key];
                var article = lbuArticle[lbu.cid];
                if (article) {
                    lbu.articleImage = article.imageUrl === "missing" ? undefined : article.imageUrl;
                    lbu.articleUrl = article.url;
                    lbu.articleTagline = article.articleName;
                } else {
                    lbu.articleTagline = lbu.name + " säljer " + q;
                }
                lbu.url = lbu.url === "missing" || !lbu.url ? undefined : lbu.url;
                if (lbu.name) {
                    lbu.showUrl = lbu.articleImage ? lbu.articleImage : undefined ; /*lbu.logoUrl ? lbu.logoUrl : "/img/sh/lorempixel.jpg";*/
                    lbu.linkUrl = lbu.articleUrl ? lbu.articleUrl : lbu.url;
                    if (!cid) {
                        delete lbu.phone;
                        delete lbu.url;
                    } else {
                        lbu.postArea = (lbu.postNumber + ' ' + lbu.postArea);
                    }
                    ['articleImage', 'articleUrl', 'logoUrl', 'businessCategory', 'phrases', 'categoryNames',
                        'categoryProducts', 'postNumber', 'openingHours', 'phone2'].forEach(function (p) {
                            delete lbu[p]; // Remove all stuff we don't need
                        });
                    if (!cid || cid === lbu.cid) {
                        lbus.push(lbu);
                    }
                }
            }
            if (cid) {
                result.articles = lbuArticles.slice(0,50);
            }
            result.metadata.hits = lbus.length;
            result.lbus = lbus;
            res.json(result);
        }, errorHandler);
    }, errorHandler);
};

var errorHandler = function (error, res) {
    console.trace(error.message);
    res.status(error.status).json(error.message);
}
