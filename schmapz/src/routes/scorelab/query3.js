
exports.articleQuery = function(parameterMap) {
    return {
        index: 'lbu',
        type: 'article',
        "size":4000,
        explain:true,
        body: {
            "query": {
                "query_string" : {
                    "fields": defaultFields(parameterMap.articleFields),
                    "query" :  splitQueryString(parameterMap.q)
                }
            }
        }
    };
};

exports.lbuQuery = function(parameterMap, articles) {
    console.log("lbu.parameterMap: %j", parameterMap);
    return {
        index: 'lbu',
        type: 'lbu',
        "size":4000,
        explain:true,
        body: {
            "query":{
                "bool": {
                    "should": [
                        {
                            "query_string":{
                                "fields": defaultFields(parameterMap.lbuFields),
                                "query":splitQueryString(parameterMap.q)
                            }
                        },
                        {
                            "terms": {
                                "cid": articles,
                                boost: parameterMap.lbuFields.article                                
                            }
                        }                        
                    ]
                }
            }
        }
    };
};


var splitQueryString = function(q) {
    return (''+q).trim()
        .split(/\s+/)
        .map(function(x){return x+"*";}) // Only star after each word
        .join(' AND ');
};


var defaultFields = function(parameterMap) {
    var fields = [];
    for(param in parameterMap) {
        fields.push( param+"^"+parameterMap[param] );
    }
    return fields;
};
