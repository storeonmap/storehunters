'use strict';

var client = new require('elasticsearch').Client();
var sorter = require('../../lbu/sorter');
//var request = require('request');
//var requestify = require('requestify');
//var async = require('async');
//var _ = require('lodash');


// TODO: Clean up this - mala

//exports.getArticle = (req, res) => {
//    let id = req.params.cid;
//    client.get({
//        index: 'lbu',
//        type: 'article',
//        id: id
//    }).then(function (body) {
//        res.json(body);
//    }, function (error) {
//        console.trace(error.message);
//        res.status(error.status).json(error.message)
//    });
//};

//exports.get = function (req, res) {
//    var id = req.params.cid;
//    request.get(
//        'http://elasticsearch:9200/lbu/article/' + id,
//        function (error, response, body) {
//            if (!error && response.statusCode == 200) {
//                var j = JSON.parse(body);
//                var r = j._source;
//                //r.q = q;
//                r.lbu = req.params.lbu;
//                r.lbuName = req.params.lbuName;
//                console.log('GET article: ' + JSON.stringify(r, null, 4));
//                r.deb = JSON.stringify(r, null, 4);
//
//
//                res.render('article', {hit: r});
//            } else {
//                console.log('ERROR (exports.get) response from elastic: ' + response.statusCode + ', ' + error);
//                console.trace(error.message);
//
//                res.redirect('/articlelist/' + 1 + '/' + 2)
//            }
//        }
//    );
//};

//var createArticle = body => {
//    let article = {
//        name: body._source.articleName[0],
//        price: body._source.price === 'missing' ? undefined : body._source.price,
//        url: body._source.url,
//        image: body._source.imageUrl === 'missing' ? undefined : body._source.imageUrl,
//        text: body._source.textEx === 'missing' ? '' : body._source.textEx.length > 140 ? body._source.textEx.slice(0, 140) + ' ...' : body._source.textEx,
//        breadCrumb: body._source.breadCrumb === 'missing' ? undefined : body._source.breadCrumb
//        //id: body._source.cid,
//        //score: body._score
//    };
//    return article;
//};

//exports.getArticles = (req, res) => {
//    console.log('ALBU2: ' + req.params.lbu);
//    var lbuId = req.params.id;
//    var query = {
//        index: 'lbu',
//        type: 'article',
//        'q': req.params.q,
//        'size': 400
//    };
//    console.log('query:' + JSON.stringify(query));
//
//    var articles = [];
//    client.search(query).then(function (body) {
//        body.hits.hits.forEach(function (hit) {
//            //console.log(hit._source.articleName);
//            let article = {
//                //name : body._source.articleName[0],
//                //price: body._source.price === 'missing' ? undefined : body._source.price,
//                url: body._source.url,
//                image: body._source.imageUrl === 'missing' ? undefined : body._source.imageUrl,
//                text: body._source.textEx.length > 140 ? body._source.textEx.slice(0, 140) + ' ...' : body._source.textEx,
//                breadCrumb: body._source.breadCrumb === 'missing' ? undefined : body._source.breadCrumb,
//                //id: body._source.cid,
//                //score: body._score
//
//            };
//            articles.push(article);
//            //articles.push(createArticle(hit));
//        });
//        body.hits = undefined;
//        body.articles = articles;
//        res.json(body);
//    }, function (error) {
//        console.trace(error.message);
//        res.status(error.status).json(error.message)
//    });
//};

// http://192.168.1.5:4000/api/scorelab4/lbu/1411408681542/articles/Backventil
exports.articlesSearch = (req, res) => {
    var lbuId = req.params.id;
    var q = req.params.q ? req.params.q.toLocaleString().toLocaleLowerCase() : '';
    //var p1 = ',{"wildcard":{"_all":"';
    //var p2 = '*"}}';
    //var esq2 = `${p1}${q.split(/\s+/).join(p2 + p1)}${p2}`;

    var lbuQuery = [{
        "match": {
            "lbu": lbuId
        }
    }];

    var wildcardWords = q.trim()
        .split(/\s+/)
        .map(function (x) {
            return {"wildcard": {"_all": x + "*"}};
        });

    var must = lbuQuery.concat(wildcardWords);

    var query = {
        index: 'lbu',
        type: 'article',
        body: {
            "query": {
                "bool": {
                    "must": must
                }
            }
        },
        size: 50
    };
    //console.log("QI: " + JSON.stringify(query, null, 4));

    client.search(query)
        .then(body => {
            let articles = body.hits.hits.map(hit => {
                return {
                    name: hit._source.articleName[0],
                    url: hit._source.url,
                    image: hit._source.imageUrl && hit._source.imageUrl  !== 'missing' ? hit._source.imageUrl : '',
                    textEx: hit._source.textEx && hit._source.textEx.length > 140 ? hit._source.textEx.substr(0, 140).replace(/ [^ ]*$/, ' ...') : hit._source.textEx.replace(/missing/,''),
                    text: hit._source.textEx !== 'missing' ? hit._source.textEx : undefined,
                    price: hit._source.price !== 'missing' ? hit._source.price : ''
                };
            });
            res.json(sorter.sortMapArrayOnField('name', articles));
        }, function (error) {
            console.trace(error.message);
            res.status(error.status).json(error.message)
        });
};

// http://192.168.1.5:4000/api/scorelab4/lbu/1411408681542
exports.getLbu = (req, res) => {
    var id = req.params.id;
    client.get({
        index: 'lbu',
        type: 'lbu',
        id: id
    }).then(function (body) {
        let url = body._source.url && body._source.url !== 'missing' ? body._source.url : '';
        let displayUrl = url.replace(/https?:\/\//, '').replace(/\/$/, '');
        let lbu = {
            name: body._source.name,
            street: body._source.street,
            postNumber: body._source.postNumber,
            postArea: body._source.postArea,
            phone: body._source.phone,
            url: url,
            displayUrl: displayUrl,
            logoUrl: body._source.logoUrl,
            location: body._source.location,
            webstore: body._source.webstore,
            cid: id
        };
        res.json(lbu);
    }, function (error) {
        console.trace(error.message);
        res.status(error.status).json(error.message)
    });
};
