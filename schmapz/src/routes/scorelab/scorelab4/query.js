


'use strict';

(function () {

   var _ = require('lodash');
   var elasticsearch = require('./Elasticsearch.js');
   
   function searchLbuByArticle(httpRequest, httpResponse) {
      var parameterMap = httpRequest.body.parameterMap;
      elasticsearch.queryLbuByArticle( parameterMap, queryCallback( httpResponse ) );
   }
   
   function searchLbuByCollection(httpRequest, httpResponse) {
      var parameterMap = httpRequest.body.parameterMap;
      elasticsearch.queryLbuByCollection( parameterMap, queryCallback( httpResponse ) );
   }
   
   function searchLbu(httpRequest, httpResponse) {
      var parameterMap = httpRequest.body.parameterMap;
      elasticsearch.queryLbuByLbu( parameterMap, lbuQueryCallback( httpResponse ) );
   }

   
   var queryCallback = _.curry( function(httpResponse, error, jsonResult) {
      if(! handleError(error, httpResponse) ) {
         httpResponse.json(jsonResult);
      }
   });


   var lbuQueryCallback = _.curry( function(httpResponse, error, jsonResult) {
      if(! handleError(error, httpResponse) ) {
         _(jsonResult.hits.hits)
            .forEach(stripLbu);         
         httpResponse.json(jsonResult);
      }
   });


   function stripLbu(lbu) {
      delete lbu._source.businessCategory;
      delete lbu._source.phrases;
      delete lbu._source.categoryProducts;
   }
   
   function handleError(error, httpResponse) {
      if(error) {
         console.log("Error", error);
         httpResponse.status(500);
         httpResponse.json( {error: error} );
         return true;
      } else {
         return false;
      }
   }

   exports.searchLbuByArticle=searchLbuByArticle;
   exports.searchLbuByCollection=searchLbuByCollection;
   exports.searchLbu=searchLbu;
   
}());
