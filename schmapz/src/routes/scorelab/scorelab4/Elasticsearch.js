'use strict';

(function () {

   var _ = require('lodash');
   
   var INDEX_LBU='lbu';
   
   var elasticsearch = require('elasticsearch');
   var client = new elasticsearch.Client({
      host: 'elasticsearch:9200'
//            , log: 'trace'
   });


   function queryLbuByArticle( parameterMap, callback ) {
      console.log("queryLbuByArticle %j",articleQuery(parameterMap));
      client.search(
         articleQuery(parameterMap),
         callback );
   }
   
   
   function articleQuery(parameterMap) {
      return {
         index: 'lbu',
         type: 'article',
         "size":4000,
         explain:false,
         body: {
            "query": {
               "query_string" : {
                  "fields": defaultFields(parameterMap.scoreSettings),
                  "query" :  splitQueryString(parameterMap.q)
               }
            },
            "filter" : {
               "bool" : {
                  "must" : {
                     "term" : { "type" : "article" }
                  }
               }
            }            
         }
      };
   };
   
   
   
   function queryLbuByCollection( parameterMap, callback ) {
      client.search(
         collectionQuery(parameterMap),
         callback );
   }

   
   function collectionQuery(parameterMap) {
      return {
         index: 'lbu',
         type: 'article',
         "size":4000,
         explain:false,
         body: {
            "query": {
               "query_string" : {
                  "fields": defaultFields(parameterMap.scoreSettings),
                  "query" :  splitQueryString(parameterMap.q)
               }
            },
            "filter" : {
               "bool" : {
                  "must" : {
                     "term" : { "type" : "collection" }
                  }
               }
            }            
         }
      };
   };


   
   function queryLbuByLbu( parameterMap, callback ) {
      console.log('queryLbuByLbu query %j',lbuQuery(parameterMap));
      client.search(
         lbuQuery(parameterMap),
         callback );
   }

   function lbuQuery(parameterMap) {
      var langLbu = (parameterMap.lang == "en") ? 'lbu_en' : 'lbu';
      return {
         index: langLbu,
         type: 'lbu',
         "size":4000,
         explain:false,
         body: {
            "query": {
               "query_string" : {
                  "fields": defaultFields(parameterMap.scoreSettings),
                  "query" :  splitQueryString(parameterMap.q)
                  //,"analyzer" : "my_analyzer"
               }
            },
            "highlight": {
               "fields" : {
                  "categoryProducts" : {},
                  "categoryNames" : {},
                  "phrases.brand": {},
                  "phrases.value": {},
                  "phrases.product": {},
                  "name": {}
               }
            }
         }
      };
   };


   
   var splitQueryString = function(q) {
      return (''+q).trim()
         .split(/\s+/)
         .filter(filterWords) 
         .map(function(x){return x+"*";}) // Only star after each word
      //         .map(function(x){return x;}) // Only star after each word
         .join(' AND ');
   };

   // Remove word "Bromma" if last in multi-word query string 
   function filterWords(value, index, collection) {      
      return (index === 0) ||  (index < (_.size(collection)-1)) || ( _.lowerCase(value) !== 'bromma' );
   }



   // function test(str) {
   //    console.log(str, ' --> ', splitQueryString(str));
   // }
   // test('bromma');
   // test('Bromma');
   // test('BROMMA');
   // test('bromma cykel');
   // test('Bromma cykel');
   // test('BROMMA cykel');
   // test('cykel bromma');
   // test('cykel Bromma');
   // test('cykel BROMMA');
   // test('cykel skidor bromma');
   // test('cykel skidor Bromma');
   // test('cykel skidor BROMMA');
   // test('cykel bromma skidor' );
   // test('cykel Bromma skidor');
   // test('cykel BROMMA skidor');
   

   

   var defaultFields = function(scoreSettings) {
      var fields = [];
      for(var param in scoreSettings) {
         fields.push( param+"^"+scoreSettings[param] );
      }
      return fields;
   };

   exports.queryLbuByArticle=queryLbuByArticle;
   exports.queryLbuByCollection=queryLbuByCollection;
   exports.queryLbuByLbu=queryLbuByLbu;
   
}());
